﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TerminalSharedDataModels.Attributes {

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class DisplayColorAttribute : Attribute {

        readonly byte A;
        readonly byte R;
        readonly byte G;
        readonly byte B;


        public Color color { get {
                return Color.FromArgb( A, R, G, B );
            } }

        public DisplayColorAttribute(byte _A, byte _R, byte _G, byte _B) {
            this.A = _A;
            this.R = _R;
            this.G = _G;
            this.B = _B;
        }

    }

}
