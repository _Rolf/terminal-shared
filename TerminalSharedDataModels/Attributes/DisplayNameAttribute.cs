﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSharedDataModels.Attributes {

    [AttributeUsage(AttributeTargets.All)]
    public class DisplayNameAttribute : Attribute {

        public string DisplayName;

        public DisplayNameAttribute(string _DisplayName) {
            this.DisplayName = _DisplayName;
        }

    }
}
