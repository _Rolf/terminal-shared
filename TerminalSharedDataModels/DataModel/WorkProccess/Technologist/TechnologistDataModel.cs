﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSharedDataModels.DataModel;
using TerminalSharedDataModels.Attributes;
using TerminalSharedDataModels.DataModel.Wrapper;

namespace TerminalSharedDataModels.DataModel.WorkProccess.Technologist {

    [Serializable]
    public class TechnologistDataModel {

        public TechnologistDataModel() { }

        public TechnologistDataModel(TechnologistDataModel inst) {
            this.Name = inst.Name;
            this._LengthStopperDecriptor = new LengthStopperDecriptor( inst._LengthStopperDecriptor );
            this._AngleAdjustmentDecriptor = new AngleAdjustmentDecriptor(inst._AngleAdjustmentDecriptor);
            this._SawDecriptor = new SawDecriptor(inst._SawDecriptor);
            this._SpindleZSDecriptor = new SpindleZSDecriptor(inst._SpindleZSDecriptor);
        }


        [ReadOnly]
        [HideInView]
        [DisplayName("Имя операции")]
        public string Name { get; set; }

        [DisplayColor(0xFF,0x2E,0xCC,0x71)]
        [DisplayName("Параметры упора заготовки")]
        public LengthStopperDecriptor _LengthStopperDecriptor { get; set; }
            = new LengthStopperDecriptor();

        [DisplayColor( 0xFF, 0xE7,0x4C, 0x3C )]
        [DisplayName("Параметры регулировки угла")]
        public AngleAdjustmentDecriptor _AngleAdjustmentDecriptor { get; set; }
            = new AngleAdjustmentDecriptor();

        [DisplayColor( 0xFF, 0x34, 0x98, 0xDB )]
        [DisplayName("Параметры пилы")]
        public SawDecriptor _SawDecriptor { get; set; }
            = new SawDecriptor();

        [DisplayColor( 0xFF, 0xE6, 0x7E, 0x22 )]
        [DisplayName("Параметры ZS шпинделя")]
        public SpindleZSDecriptor _SpindleZSDecriptor { get; set; }
            = new SpindleZSDecriptor();


    }


    [Serializable]
    public class LengthStopperDecriptor {

        public LengthStopperDecriptor() { }

        public LengthStopperDecriptor(LengthStopperDecriptor inst) {
            this.PositionSpeed      = new DecimalMeasureWrapper( inst.PositionSpeed );
            this.ReferencingSpeed   = new DecimalMeasureWrapper( inst.ReferencingSpeed );
            this.BringingSpeed      = new DecimalMeasureWrapper( inst.BringingSpeed );
            this.OutSpeed_1         = new DecimalMeasureWrapper( inst.OutSpeed_1 );
            this.OutSpeed_2         = new DecimalMeasureWrapper( inst.OutSpeed_2 );
            this.MaximumSpeed       = new DecimalMeasureWrapper( inst.MaximumSpeed );
            this.RampTime           = new DecimalMeasureWrapper( inst.RampTime );
            this.PositioningWindow  = new DecimalMeasureWrapper( inst.PositioningWindow );
            this.ReferencePoint     = new DecimalMeasureWrapper( inst.ReferencePoint );
            this.MinEndStopper      = new DecimalMeasureWrapper( inst.MinEndStopper );
            this.MaxEndStopper      = new DecimalMeasureWrapper( inst.MaxEndStopper );
            this.PositioningTime    = new DecimalMeasureWrapper( inst.PositioningTime );
            this.OpeningTime        = new DecimalMeasureWrapper( inst.OpeningTime );
            this.ClosingTime        = new DecimalMeasureWrapper( inst.ClosingTime );
            this.TablingTime        = new DecimalMeasureWrapper( inst.TablingTime );
        }


        [DisplayName( "Скорость позиционирования" )]
        public DecimalMeasureWrapper PositionSpeed { get; set; } =
            new DecimalMeasureWrapper( 3000, "0.1 mm/s", 50, 30000 );

        [DisplayName( "Скорость референцирования" )]
        public DecimalMeasureWrapper ReferencingSpeed { get; set; } =
            new DecimalMeasureWrapper( 1500, "0.1 mm/s", 10, 2000 );

        [DisplayName( "Скорость подъездки" )]
        public DecimalMeasureWrapper BringingSpeed { get; set; } =
            new DecimalMeasureWrapper( 2000, "0.1 mm/s", 50, 6000 );

        [DisplayName( "Скорость выдачи 1" )]
        public DecimalMeasureWrapper OutSpeed_1 { get; set; } =
            new DecimalMeasureWrapper( 1000, "0.1 mm/s", 10, 1000 );

        [DisplayName( "Скорость выдачи 2" )]
        public DecimalMeasureWrapper OutSpeed_2 { get; set; } =
            new DecimalMeasureWrapper( 2000, "0.1 mm/s", 10, 2000 );

        [DisplayName( "Максимальная скорость" )]
        public DecimalMeasureWrapper MaximumSpeed { get; set; } =
            new DecimalMeasureWrapper( 5000, "0.1 mm/s", 10, 12300 );

        [DisplayName( "Время разгона" )]
        public DecimalMeasureWrapper RampTime { get; set; } =
            new DecimalMeasureWrapper( 20, "10ms", 0, 3000 );

        [DisplayName( "Окно позиционирования" )]
        public DecimalMeasureWrapper PositioningWindow { get; set; } =
            new DecimalMeasureWrapper( 0.1m, "mm", 0.01m, 1 );

        [DisplayName( "Точка референцирования" )]
        public DecimalMeasureWrapper ReferencePoint { get; set; } =
            new DecimalMeasureWrapper( 2253.3m, "mm", 0, 4000 );


        // Выход за границы. Перепроверить
        [DisplayName( "Начальный концевик" )]
        public DecimalMeasureWrapper MinEndStopper { get; set; } =
            new DecimalMeasureWrapper( 220 , "mm", 0, 20 );

        [DisplayName( "Конечный концевик" )]
        public DecimalMeasureWrapper MaxEndStopper { get; set; } =
            new DecimalMeasureWrapper( 2253.3m , "mm", 0, 5 );

        [DisplayName( "Время позиционирования" )]
        public DecimalMeasureWrapper PositioningTime { get; set; } =
            new DecimalMeasureWrapper( 20, "s", 0, 20 );

        [DisplayName( "Время открытия" )]
        public DecimalMeasureWrapper OpeningTime { get; set; } =
            new DecimalMeasureWrapper( 4, "s", 2, 10 );
        [DisplayName( "Время закрытия" )]
        public DecimalMeasureWrapper ClosingTime { get; set; } =
            new DecimalMeasureWrapper( 4, "s", 2, 10 );

        [DisplayName( "Время заготовки на столе" )]
        public DecimalMeasureWrapper TablingTime { get; set; } =
            new DecimalMeasureWrapper( 8, "s", 0, 20 );

    }

    [Serializable]
    public class AngleAdjustmentDecriptor {
        public AngleAdjustmentDecriptor() { }
        public AngleAdjustmentDecriptor(AngleAdjustmentDecriptor inst) {
            this.PositionSpeed = new DecimalMeasureWrapper(inst.PositionSpeed);
            this.OutSpeed_1 = new DecimalMeasureWrapper(inst.OutSpeed_1);
            this.OutSpeed_2 = new DecimalMeasureWrapper(inst.OutSpeed_2);
            this.ReferencingSpeed = new DecimalMeasureWrapper(inst.ReferencingSpeed);
            this.MaximumSpeed = new DecimalMeasureWrapper(inst.MaximumSpeed);
            this.PositioningWindow = new DecimalMeasureWrapper(inst.PositioningWindow);
            this.ReferencePoint = new DecimalMeasureWrapper(inst.ReferencePoint);
            this.MinEndStopper = new DecimalMeasureWrapper(inst.MinEndStopper);
            this.MaxEndStopper = new DecimalMeasureWrapper(inst.MaxEndStopper);
            this.PositioningTime = new DecimalMeasureWrapper(inst.PositioningTime);
            this.NullOffset = new DecimalMeasureWrapper(inst.NullOffset);
        }

        [DisplayName("Скорость позиционирования")]
        public DecimalMeasureWrapper PositionSpeed { get; set; } =
            new DecimalMeasureWrapper(30, "0.1 mm/s", 5, 50);
        [DisplayName("Скорость выдачи 1")]
        public DecimalMeasureWrapper OutSpeed_1 { get; set; } =
            new DecimalMeasureWrapper(50, "0.1 mm/s", 5, 20);
        [DisplayName("Скорость выдачи 2")]
        public DecimalMeasureWrapper OutSpeed_2 { get; set; } =
            new DecimalMeasureWrapper(100, "0.1 mm/s", 5, 40);
        [DisplayName("Скорость референцирования")]
        public DecimalMeasureWrapper ReferencingSpeed { get; set; } =
            new DecimalMeasureWrapper(100, "0.1 mm/s", 5, 50);
        [DisplayName("Максимальная скорость")]
        public DecimalMeasureWrapper MaximumSpeed { get; set; } =
            new DecimalMeasureWrapper(100, "0.1 mm/s", 5, 50);
        [DisplayName("Время разгона")]
        public DecimalMeasureWrapper RampTime { get; set; } =
            new DecimalMeasureWrapper(15, "s", 0.001m, 200);
        [DisplayName("Окно позиционирования")]
        public DecimalMeasureWrapper PositioningWindow { get; set; } =
            new DecimalMeasureWrapper(0.2m, "mm", 0.01m, 1);
        [DisplayName("Точка референцирования")]
        public DecimalMeasureWrapper ReferencePoint { get; set; } =
            new DecimalMeasureWrapper(24.33m, "mm", 0, 180);
        [DisplayName("Начальный концевик")]
        public DecimalMeasureWrapper MinEndStopper { get; set; } =
            new DecimalMeasureWrapper(-75.5m, "mm", 0, 50);

        [DisplayName("Конечный концевик")]
        public DecimalMeasureWrapper MaxEndStopper { get; set; } =
            new DecimalMeasureWrapper(75.5m, "mm", 0, 180);
        [DisplayName("Время позиционирования")]
        public DecimalMeasureWrapper PositioningTime { get; set; } =
            new DecimalMeasureWrapper(50, "s", 0, 100);
        [DisplayName("Смещение нуля")]
        public DecimalMeasureWrapper NullOffset { get; set; } =
            new DecimalMeasureWrapper(76.35m, " ", 1000, 1000);
    }

    [Serializable]
    public class SawDecriptor {
        public SawDecriptor() { }
        public SawDecriptor(SawDecriptor inst) {
            this.ReferencePoint = new DecimalMeasureWrapper(inst.ReferencePoint);
            this.TurnOffPoint = new DecimalMeasureWrapper(inst.TurnOffPoint);
            this.CuttOffPoint = new DecimalMeasureWrapper(inst.CuttOffPoint);
            this.LengthCycle = new DecimalMeasureWrapper(inst.LengthCycle);
            this.UpperLimitPoint = new DecimalMeasureWrapper(inst.UpperLimitPoint);
            this.lowerLimitPoint = new DecimalMeasureWrapper(inst.lowerLimitPoint);
            this.PositioningWindow = new DecimalMeasureWrapper(inst.PositioningWindow);
        }

        [DisplayName("Точка ориентирования")]
        public DecimalMeasureWrapper ReferencePoint { get; set; } =
            new DecimalMeasureWrapper(108.2m, "mm", 100, 150);
        [DisplayName("Точка выключения")]
        public DecimalMeasureWrapper TurnOffPoint { get; set; } =
            new DecimalMeasureWrapper(0.18m, "mm", 0, 4);
        [DisplayName("Точка пред. обрезания")]
        public DecimalMeasureWrapper CuttOffPoint { get; set; } =
            new DecimalMeasureWrapper(1, "mm", 0, 4);
        [DisplayName("Длина цикла")]
        public DecimalMeasureWrapper LengthCycle { get; set; } =
            new DecimalMeasureWrapper(2, "mm", 0, 4);
        [DisplayName("Верхний предел")]
        public DecimalMeasureWrapper UpperLimitPoint { get; set; } =
            new DecimalMeasureWrapper(145, "mm", 0, 145);
        [DisplayName("Нижний предел")]
        public DecimalMeasureWrapper lowerLimitPoint { get; set; } =
            new DecimalMeasureWrapper(-2, "mm", 0, -5);
        [DisplayName("Окно положения")]
        public DecimalMeasureWrapper PositioningWindow { get; set; } =
            new DecimalMeasureWrapper(0.1m, "mm", 0, 0.5m);
    }

    [Serializable]
    public class SpindleZSDecriptor {
        public SpindleZSDecriptor() { }
        public SpindleZSDecriptor(SpindleZSDecriptor inst) {
            this.PositionSpeed = new DecimalMeasureWrapper(inst.PositionSpeed);
            this.OutSpeed_1 = new DecimalMeasureWrapper(inst.OutSpeed_1);
            this.OutSpeed_2 = new DecimalMeasureWrapper(inst.OutSpeed_2);
            this.ReferencingSpeed = new DecimalMeasureWrapper(inst.ReferencingSpeed);
            this.MaximumSpeed = new DecimalMeasureWrapper(inst.MaximumSpeed);
            this.RampTime = new DecimalMeasureWrapper(inst.RampTime);
            this.PositioningWindow = new DecimalMeasureWrapper(inst.PositioningWindow);
            this.ReferencePoint = new DecimalMeasureWrapper(inst.ReferencePoint);
            this.Stopper_1 = new DecimalMeasureWrapper(inst.Stopper_1);
            this.Stopper_2 = new DecimalMeasureWrapper(inst.Stopper_2);
            this.PositioningTime = new DecimalMeasureWrapper(inst.PositioningTime);
        }
        [DisplayName("Скорость позиционирования")]
        public DecimalMeasureWrapper PositionSpeed { get; set; } =
            new DecimalMeasureWrapper(2500, "0.1 mm/s", 10, 2500);
        [DisplayName("Скорость выдачи 1")]
        public DecimalMeasureWrapper OutSpeed_1 { get; set; } =
            new DecimalMeasureWrapper(300, "0.1 mm/s", 10, 2500);
        [DisplayName("Скорость выдачи 2")]
        public DecimalMeasureWrapper OutSpeed_2 { get; set; } =
            new DecimalMeasureWrapper(600, "0.1 mm/s", 10, 2500);
        [DisplayName("Скорость референцирования")]
        public DecimalMeasureWrapper ReferencingSpeed { get; set; } =
            new DecimalMeasureWrapper(1200, "0.1 mm/s", 10, 2500);
        [DisplayName("Максимальная скорость")]
        public DecimalMeasureWrapper MaximumSpeed { get; set; } =
            new DecimalMeasureWrapper(2500, "0.1 mm/s", 10, 4000);
        [DisplayName("Время разгона")]
        public DecimalMeasureWrapper RampTime { get; set; } =
            new DecimalMeasureWrapper(100, "10 ms", 10, 3000);
        [DisplayName("Окно позиционирования")]
        public DecimalMeasureWrapper PositioningWindow { get; set; } =
            new DecimalMeasureWrapper(0.5m, "mm", 0.01m, 1);
        [DisplayName("Точка референцирования")]
        public DecimalMeasureWrapper ReferencePoint { get; set; } =
            new DecimalMeasureWrapper(552.02m, "mm", 500, 2000);

        [DisplayName("Концевик 1")]
        public DecimalMeasureWrapper Stopper_1 { get; set; } =
            new DecimalMeasureWrapper(4.5m, "mm", 0, 2000);
        [DisplayName("Концевик 2")]
        public DecimalMeasureWrapper Stopper_2 { get; set; } =
            new DecimalMeasureWrapper(600, "mm", 0, 2000);
        [DisplayName("Время позиционирования")]
        public DecimalMeasureWrapper PositioningTime { get; set; } =
            new DecimalMeasureWrapper(50, "s", 3, 10);
    }
}
