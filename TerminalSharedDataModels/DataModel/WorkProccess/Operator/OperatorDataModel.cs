﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSharedDataModels.DataModel;
using TerminalSharedDataModels.DataModel.Wrapper;

namespace TerminalSharedDataModels.DataModel.WorkProccess.Operator {
    public class OperatorDataModel {

        //[Attributes.HideInView]
        [Attributes.DisplayName("Операция")]
        public Technologist.TechnologistDataModel Operation { get; set; }

        [Attributes.DisplayName ( "Глубина шипа" )]
        public DecimalMeasureWrapper SpikeDepth { get; set; }
            = new DecimalMeasureWrapper ( 0, "mm", 0, 1000 );

        [Attributes.DisplayName ( "Угол" )]
        public DecimalMeasureWrapper Angle { get; set; }
            = new DecimalMeasureWrapper ( 0, "°", 0, 90 );


        [Attributes.DisplayName ( "Длина заготовки" )]
        public DecimalMeasureWrapper Legth { get; set; }
            = new DecimalMeasureWrapper ( 280, "mm", 280, 3600 );

        [Attributes.DisplayName ( "Номер торца" )]
        public OperatorFaceNumberModel FaceNumber { get; set; }
            = new OperatorFaceNumberModel ( );

    }

    public class OperatorFaceNumberModel {
        public uint number { get; set; } = 1;
    }

}
