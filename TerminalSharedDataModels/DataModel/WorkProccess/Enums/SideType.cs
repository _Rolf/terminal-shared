﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSharedDataModels.DataModel.WorkProccess.Enums {
    public enum SideType : uint {
        INNER,
        OUTER
    }
}
