﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSharedDataModels.DataModel.Wrapper {

    [Serializable]
    public class DecimalMeasureWrapper {

        private decimal _val;
        public decimal value {
            get {
                return _val;
            } set {
                _val = value;
            } }

        public decimal valueMax { get; set; }
        public decimal valueMin { get; set; }

        public string measureUnit { get; set; }


        public DecimalMeasureWrapper() { }

        public DecimalMeasureWrapper(DecimalMeasureWrapper d) {
            this.valueMax = d.valueMax;
            this.valueMin = d.valueMin;
            this.value = d.value;
            this.measureUnit = d.measureUnit;
        }

        public DecimalMeasureWrapper(decimal _value, string _measureUnit , decimal _valueMinLimit, decimal _valueMaxLimit) {
            this.valueMax = _valueMaxLimit;
            this.valueMin = _valueMinLimit;

            this.value = _value;
            this.measureUnit = _measureUnit;
        }

        public bool isOkLimit(decimal newval) {
            return newval >= valueMin && newval <= valueMax;
        }

    }
}
