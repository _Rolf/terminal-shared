﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSharedDataModels.DataModel.DataMapper.BinaryMapper {

    [Serializable]
    public class BinaryMapperDataModel {

        [Serializable]
        public class BinaryMapperPropertyDescriptionModel {
            public string PropName;
            public Type PropTargetType;
        }

        public IList<BinaryMapperPropertyDescriptionModel> PropertiesList 
            = new List<BinaryMapperPropertyDescriptionModel>();

    }
}
