﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSharedDataModels.DataModel {
    /// <summary>
    /// Класс является композицией всех глобальных Data Models для облегчения доступа к ним и их хранения
    /// </summary>
    public class GlobalDataModelHeaver {

        /// <summary>
        /// Синглтон
        /// </summary>
        public static GlobalDataModelHeaver instance { get; private set; }

        static GlobalDataModelHeaver() {
            new GlobalDataModelHeaver();
        }

        public GlobalDataModelHeaver() {
            User = new User.UserModel();

            instance = this;
        }

        public User.IUserModel User { get; set; }
    }
}
