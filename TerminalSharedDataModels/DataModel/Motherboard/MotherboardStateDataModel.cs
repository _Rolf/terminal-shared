﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSharedDataModels.DataModel.Motherboard {

    [DataContract]
    public class MotherboardStateDataModel {

        [DataMember]
        [Attributes.DisplayName( "Соединение" )]
        public bool bConnectionOK {
            get; set;
        } = false;

    }

}
