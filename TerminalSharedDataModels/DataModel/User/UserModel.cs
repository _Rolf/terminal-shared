﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSharedDataModels.DataModel.Enums;

namespace TerminalSharedDataModels.DataModel.User {
    public class UserModel : IUserModel {

        public UserRole Role { get; set; } = UserRole.USER;

    }
}
