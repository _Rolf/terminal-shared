﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TerminalSharedDataModels.DataModel.Motherboard;

namespace TerminalMotherboardAdapterService.Service.MotherboardStateService {

    [ServiceContract( Namespace = "TerminalMotherboardAdapterService.Service.MotherboardStateService" )]
    public interface IMotherboardStateService {

        [OperationContract]
        MotherboardStateDataModel GetState();

    }

}
