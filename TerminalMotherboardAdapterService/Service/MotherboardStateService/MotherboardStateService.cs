﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSharedDataModels.DataModel.Motherboard;

namespace TerminalMotherboardAdapterService.Service.MotherboardStateService {
    class MotherboardStateService : IMotherboardStateService {

        public MotherboardStateDataModel GetState() {
            return new MotherboardStateDataModel() {
                bConnectionOK = true
            };
        }

    }
}
