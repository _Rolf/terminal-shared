﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

using TerminalMotherboardAdapterService;

namespace TerminalMotherboardAdapterService.Service {
    public class ServiceHostInitializer<T, IT>
        where T : class {

        protected ServiceHost host { get; set; }

        public ServiceHostInitializer(int port){
            var uri = new Uri( string.Format( "http://localhost:{0}/{1}", port, typeof( T ).Namespace.Split( '.' ).Last() ) );

            host = new ServiceHost( typeof(T), uri );

            host.AddServiceEndpoint( typeof( IT ), new WSHttpBinding(), typeof( T ).Name );

            var smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;

            host.Description.Behaviors.Add( smb );
        }

        public void Open() {
            host.Open();
        }

        public void Close() {
            host.Close();
        }

    }
}
