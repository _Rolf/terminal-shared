﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using TerminalMotherboardAdapterService.Service;
using TerminalMotherboardAdapterService.Service.MotherboardStateService;

namespace TerminalMotherboardAdapterService {
    class Program {
        static void Main(string[] args) {
            var host = new ServiceHostInitializer<MotherboardStateService, IMotherboardStateService>(8000);
            host.Open();

            Console.Write( "Host started..." );
            Console.ReadLine();

            host.Close();
        }
    }

}
