﻿using Ninject;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Globals.ApplicationManager;
using Terminal.View.Startup;

namespace Terminal.Presenter.Startup {

    public class StartupPresenter : IStartupPresenter {

        [Inject] public IStartupView _view { get; set; }

        public void Start() {
            _view._Display();
            //TODO: Инициализация здесь
            // -----

            ApplicationManager.Current.GetPresenter("main").Start();
            _view._Hide();
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Startup presenter started!");

        }

        public void Stop() {
            
        }
    }

}
