﻿using Ninject;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Globals.Operations.OperationsControlService;
using Terminal.View.Main;
using Terminal.View.Partials.Presenter;
using Terminal.View.Service.MenuBuilder;
using TerminalSharedDataModels.DataModel.WorkProccess.Operator;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;

namespace Terminal.Presenter.Main {
    public class MainPresenter : PresenterBase, IMainPresenter {

        [Inject] public IMainView _view { get; set; }

        public OperatorDataModel _OperatorModel;

        [Inject] public IOperationsControlService _operations { get; set; }
 
        public void Start() {
            _view.GetPresenter += () => this;

            _view._Display();
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Main presenter started!");
        }

        public void Stop() {
        }


        public void Technologist_OperationSelected(TechnologistDataModel operation) {
            foreach (var ch in Children) {
                (new RequestInterfaceAdapter(ch.Value)).Request("Technologist_OperationSelected", new object[] { operation });
            }
        }

        public void Technologist_OperationSave() {
            foreach (var ch in Children) {
                (new RequestInterfaceAdapter(ch.Value)).Request("Technologist_OperationSave", new object[] { });
            }
        }

        public void Technologist_OperationReset() {
            foreach (var ch in Children) {
                (new RequestInterfaceAdapter( ch.Value )).Request( "Technologist_OperationReset", new object[] { } );
            }
        }

        public void OperationsControl_SaveOperation() {
            foreach (var ch in Children) {
                (new RequestInterfaceAdapter( ch.Value )).Request( "OperationsControl_SaveOperation", new object[] { } );
            }
        }


    }
}
