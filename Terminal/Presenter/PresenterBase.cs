﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.View.Partials.Presenter;

namespace Terminal.Presenter {
    public class PresenterBase {

        public Dictionary<string, PartialPresenterBase> Children { get; set; } 
            = new Dictionary<string, PartialPresenterBase>();

    }
}
