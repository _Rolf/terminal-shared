﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.Presenter {
    public interface IPresenter {

        void Start();

        void Stop();

    }
}
