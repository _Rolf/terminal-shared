﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Terminal{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application{


        private void Application_Startup(object sender, StartupEventArgs e){

            LogManager.GetCurrentClassLogger().Log( LogLevel.Info, "Application Started!" );
            LogManager.GetCurrentClassLogger().Log( LogLevel.Fatal, "Application Started!" );

            var appManager = new Globals.ApplicationManager.ApplicationManager();
            appManager.GetPresenter("startup").Start();
        }
    }
}
