﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.Globals.ErrorState {

    public class ErrorState {

        public bool HasError { get; set; } = false;
        public string Error { get; set; } = null;
        public Exception ErrorException { get; set; } = null;

        public ErrorState() { }

        public ErrorState(bool _HasError, string _Error, Exception _ErrorException) {
            this.HasError = _HasError;
            this.Error = _Error;
            this.ErrorException = _ErrorException;
        }

        public override string ToString() {
            return HasError ? string.IsNullOrEmpty(Error) ? ErrorException.Message : Error : "No Error!"; 
        }

    }

}
