﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;

namespace Terminal.Globals.Operations.OperationsControlService {
    public class DirectoryOperationsControlService : IOperationsControlService {

        protected static string OPERATIONS_BASE_DIR = "/operations/";

        public List<TechnologistDataModel> Operations { get; protected set; } = new List<TechnologistDataModel>();

        protected Dictionary<FileInfo, TechnologistDataModel> OperationsFilepaths { get; set; }
            = new Dictionary<FileInfo, TechnologistDataModel>();

        public ErrorState.ErrorState Initialize() {

            try {
                Operations.Clear();
                OperationsFilepaths.Clear();

                var dir = new DirectoryInfo(System.AppDomain.CurrentDomain.BaseDirectory + OPERATIONS_BASE_DIR);

                foreach (var fileInfo in dir.EnumerateFiles()) {
                    var serializer = new XmlSerializer(typeof(TechnologistDataModel));

                    using (var stream = fileInfo.Open(FileMode.Open)) {
                        var op = serializer.Deserialize(stream) as TechnologistDataModel;
                        Operations.Add(op);
                        OperationsFilepaths.Add(fileInfo, op);
                    }
                }
            }catch (Exception ex) {
                return new ErrorState.ErrorState(true, null, ex);
            } return new ErrorState.ErrorState(false, null, null);
        }

        public ErrorState.ErrorState SaveExistingOperation(TechnologistDataModel Operation) {
            try {
                var target = OperationsFilepaths.Where(x => x.Value.Name == Operation.Name).First();

                var serializer = new XmlSerializer(typeof(TechnologistDataModel));

                using (var stream = target.Key.Open(FileMode.Create)) {
                    serializer.Serialize(stream, target.Value);
                }
            }
            catch (Exception ex) {
                return new ErrorState.ErrorState(true, null, ex);
            } return new ErrorState.ErrorState(false, null, null);
        }

        public ErrorState.ErrorState SaveNewOperation(TechnologistDataModel Operation) {
            try {

                var serializer = new XmlSerializer( typeof( TechnologistDataModel ) );

                using (var stream = new FileStream( System.AppDomain.CurrentDomain.BaseDirectory + OPERATIONS_BASE_DIR + Operation.Name + ".xml" , FileMode.Create )) {
                    serializer.Serialize( stream, Operation );
                }

                Initialize();

            } catch (Exception ex) {
                return new ErrorState.ErrorState( true, null, ex );
            }
            return new ErrorState.ErrorState( false, null, null );
        }
    }
}
