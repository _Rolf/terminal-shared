﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;

namespace Terminal.Globals.Operations.OperationsControlService {
    public interface IOperationsControlService {

        ErrorState.ErrorState Initialize();

        ErrorState.ErrorState SaveExistingOperation(TechnologistDataModel Operation);
        ErrorState.ErrorState SaveNewOperation(TechnologistDataModel Operation);

        List<TechnologistDataModel> Operations { get; }

    }
}
