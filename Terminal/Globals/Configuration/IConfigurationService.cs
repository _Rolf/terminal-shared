﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.Globals.Configuration {

    /// <summary>
    /// Интерфейс сервиса конфигурации приложения
    /// </summary>
    public interface IConfigurationService {

        ErrorState.ErrorState ReadConfiguration();

        ErrorState.ErrorState SaveConfiguration();

        object GetConfig(string configName);

        void SetConfig(string configName, object data);

        object this[string index]{get;set;}

    }
}
