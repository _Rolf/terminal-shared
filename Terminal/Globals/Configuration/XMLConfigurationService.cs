﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Terminal.Globals.ErrorState;

namespace Terminal.Globals.Configuration {

    public class XMLConfigurationService : IConfigurationService {

        protected Dictionary<string,object> ConfigData { get; set; }

        protected static string RELATIVE_CONFIG_PATH = "/config/main_config.xml";

        public object this[string index] {
            get {
                return GetConfig(index);
            }
            set {
                SetConfig(index, value);
            }
        }

        public object GetConfig(string configName) {
            return ConfigData[configName];
        }

        /// <summary>
        /// Генерирует словарь из модели данных
        /// </summary>
        /// <param name="datamodel"></param>
        /// <returns></returns>
        private Dictionary<string, object> GetConfigurationDictionary(ConfigurationDataModel datamodel) {
            var dict = new Dictionary<string, object>();

            foreach (var field in datamodel.GetType().GetFields()) 
                dict[field.Name] = field.GetValue(datamodel);

            foreach (var prop in datamodel.GetType().GetProperties())
                dict[prop.Name] = prop.GetValue(datamodel);

            return dict;
        }

        /// <summary>
        /// Генерирует модкль из словаря данных
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        private ConfigurationDataModel GetConfigurationDataModel(Dictionary<string, object> dict) {
            var model = new ConfigurationDataModel();

            foreach (var pair in dict) {
                var field = model.GetType().GetField(pair.Key);
                if (field != null) {
                    field.SetValue(model, pair.Value);
                }else {
                    var prop = model.GetType().GetProperty(pair.Key);
                    if (prop != null) {
                        prop.SetValue(model, pair.Value);
                    }
                }
            }
            return model;
        }

        public ErrorState.ErrorState ReadConfiguration() {
            try {
                var xml = File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + RELATIVE_CONFIG_PATH);
                var serializer = new XmlSerializer(typeof(ConfigurationDataModel));

                ConfigurationDataModel Data = null;
                using (var reader = new StringReader(xml)) {
                    Data = serializer.Deserialize(reader) as ConfigurationDataModel;
                }
                ConfigData = GetConfigurationDictionary(Data);

            } catch (Exception ex) {
                return new ErrorState.ErrorState(true, null, ex);
            }
            return new ErrorState.ErrorState(false, null, null);
        }

        public void SetConfig(string configName, object data) {
            ConfigData[configName] = data;
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "configuration installed!");
        }

        public ErrorState.ErrorState SaveConfiguration() {
            try {

                var confDataModel = GetConfigurationDataModel(ConfigData);
                var serializer = new XmlSerializer(typeof(ConfigurationDataModel));

                using (var stream = new FileStream(System.AppDomain.CurrentDomain.BaseDirectory + RELATIVE_CONFIG_PATH, FileMode.Create)) {
                    serializer.Serialize(stream, confDataModel);
                }

            }catch (Exception ex) {
                return new ErrorState.ErrorState(true, null, ex);
            }
            return new ErrorState.ErrorState(false, null, null);
        }
    }

}
