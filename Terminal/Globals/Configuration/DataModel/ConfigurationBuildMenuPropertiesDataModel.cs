﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.Globals.Configuration.DataModel {
    [Serializable]
    public class ConfigurationBuildMenuPropertiesDataModel {

        public int TextBoxMinWidth;
        public int LabelWidth;
        public int SPMarginBottom;
        public int SPMarginTop;
        public int LeftStep;
        public int ComboBoxWidth;
        public int ListViewWidth;
        public int FontSize;
    }
}
