﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Terminal.Globals.Configuration {

    [System.Serializable]
    [XmlRoot("Configuration")]
    public class ConfigurationDataModel {

        [XmlElement]
        public string TestConfig;

        [XmlElement]
        public DataModel.ConfigurationBuildMenuPropertiesDataModel BuildMenuProperties;

        [XmlElement]
        public DataModel.ConfigurationMainWindowDataModel ConfigurationMainWindow;

    }

}
