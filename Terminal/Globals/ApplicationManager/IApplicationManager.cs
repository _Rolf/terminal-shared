﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.Globals.ApplicationManager {

    /// <summary>
    /// Интерфейс менеджера приложения
    /// </summary>
    public interface IApplicationManager {

        /// <summary>
        /// Возвращает презентер по имени
        /// </summary>
        /// <param name="sName"></param>
        /// <returns></returns>
        Presenter.IPresenter GetPresenter(string sName);

    }
}
