﻿using Ninject;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Terminal.Presenter;

namespace Terminal.Globals.ApplicationManager {
    public sealed class ApplicationManager : IApplicationManager {

        public static IApplicationManager Current { get; private set; }

        [Inject] public Configuration.IConfigurationService _config { get; set; }
        [Inject] public Operations.OperationsControlService.IOperationsControlService _operations { get; set; }

        public bool InitializationOK { get; private set; } = false;

        public ApplicationManager() {
            Current = this;
            new Globals.Inject.InjectService.InjectService();
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Initialization global!");

            _config = Globals.Inject.InjectService.InjectService.instance.Get<Configuration.IConfigurationService>();
            var state = _config.ReadConfiguration();
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Instanced and read global configuration!");

            _operations = Globals.Inject.InjectService.InjectService.instance.Get<Operations.OperationsControlService.IOperationsControlService>();
            _operations.Initialize();
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Initialization operations!");

            if (state.HasError) {
                MessageBox.Show(state.ToString());
                InitializationOK = false;
                LogManager.GetCurrentClassLogger().Log(LogLevel.Fatal, "Global configuration failed " + state.ToString() + "!");
                System.Windows.Application.Current.Shutdown();
            }else {
                LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Global configuration successful " + state.ToString() + "!");
                InitializationOK = true;
            }

        }

        public IPresenter GetPresenter(string sName) {
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Instanced Presenter " + sName + "!");
            return Globals.Inject.InjectService.InjectService.instance.GetPresenter(sName);
        }

    }
}
