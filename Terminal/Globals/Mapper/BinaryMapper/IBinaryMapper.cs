﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSharedDataModels.DataModel.DataMapper.BinaryMapper;

namespace Terminal.Globals.Mapper.BinaryMapper {
    public interface IBinaryMapper {

        byte[] MapToBinary(BinaryMapperDataModel model, object target);

    }
}
