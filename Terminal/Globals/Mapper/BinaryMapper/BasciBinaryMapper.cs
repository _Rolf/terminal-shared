﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using TerminalSharedDataModels.DataModel;
using TerminalSharedDataModels.DataModel.DataMapper.BinaryMapper;

namespace Terminal.Globals.Mapper.BinaryMapper {
    public class BasciBinaryMapper : IBinaryMapper {

        public byte[] MapToBinary(BinaryMapperDataModel model, object target) {
            var bts = new List<byte>();

            foreach (var prop in model.PropertiesList) {
                var pinfo = target.GetType().GetProperty( prop.PropName );

                if (null == pinfo) continue;

                var value = pinfo.GetValue( target );

                bts.AddRange( BitConverter.GetBytes((UInt64)value) );

            }

            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Mapped bit massive!");

            return bts.ToArray();
        }

    }
}
