﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.Globals.Inject.InjectService {

    /// <summary>
    /// Интерфейс локатора ядра инверсии зависимостей
    /// </summary>
    public interface IInjectService {

        T Get<T>() where T : class;

        T Get<T>(string name) where T : class;

        object Get(Type t);

        Presenter.IPresenter GetPresenter(string sName);

    }
}
