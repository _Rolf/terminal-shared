﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Presenter;

namespace Terminal.Globals.Inject.InjectService {

    /// <summary>
    /// Класс обеспечивает интерфейс работы с сервисом инъекции зависимостей
    /// </summary>
    public class InjectService : IInjectService {

        /// <summary>
        /// Статичесикй интерфейс доступа, синглтон 
        /// </summary>
        public static IInjectService instance { get; protected set; }

        /// <summary>
        /// Ядро системы IOT
        /// </summary>
        public Ninject.IKernel _kernel { get; protected set; }

        /// <summary>
        /// Инициализвция ядра и его модулей
        /// </summary>
        public InjectService() {
            new Module.ModelModule();
            new Module.ViewModule();
            new Module.PresenterModule();
            new Module.ServiceModule();

            _kernel = new Ninject.StandardKernel(Module.ModuleBase.Modules.ToArray());

            instance = this;
        }

        /// <summary>
        /// Возвращает инстанс сервиса указанного типа в соотвествии с параметрами ядра
        /// </summary>
        /// <typeparam name="T">Тип сервиса</typeparam>
        /// <returns></returns>
        public T Get<T>() where T : class {
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Type " + typeof(T).ToString() + " injected!");
            return _kernel.GetService(typeof(T)) as T;
        }

        public T Get<T>(string name) where T : class {
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Type " + typeof(T).ToString() + " injected with name " + name + "!");
            return Ninject.ResolutionExtensions.Get<T>( _kernel, name );
        }

        public object Get(Type t) {
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Type " + t.Name + " injected!");
            return _kernel.GetService( t );
        }

        /// <summary>
        /// Воззращает инстанс Presenter по имени
        /// </summary>
        /// <param name="sName"></param>
        /// <returns></returns>
        public IPresenter GetPresenter(string sName) {
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Presenter " + sName + " injected!");
            return Ninject.ResolutionExtensions.Get<Presenter.IPresenter>(_kernel, sName);
        }

    }
}
