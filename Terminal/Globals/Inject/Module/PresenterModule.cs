﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Presenter;
using Terminal.Presenter.Main;
using Terminal.Presenter.Startup;

namespace Terminal.Globals.Inject.Module {
    public class PresenterModule : ModuleBase {

        public override void Load() {
            this.Kernel.Bind<IPresenter>().To<StartupPresenter>().Named("startup");
            this.Kernel.Bind<IPresenter>().To<MainPresenter>().Named("main");

        }

    }
}
