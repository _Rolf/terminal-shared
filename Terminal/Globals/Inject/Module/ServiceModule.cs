﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Globals.Configuration;
using Terminal.Globals.Mapper.BinaryMapper;
using Terminal.Globals.Operations.OperationsControlService;
using Terminal.View.Service.MenuBuilder;
using Terminal.View.Service.VirtualKeyboardInputService;
using Terminal.View.Service.VirtualKeyboardInputService.Factory;
using Terminal.View.Service.WindowHeaverService;

namespace Terminal.Globals.Inject.Module {
    public class ServiceModule : ModuleBase {

        public override void Load() {
            this.Kernel.Bind<IConfigurationService>().To<XMLConfigurationService>().InSingletonScope();
            this.Kernel.Bind<IOperationsControlService>().To<DirectoryOperationsControlService>().InSingletonScope();

            this.Kernel.Bind<IMenuBuilder>().To<BasicMenuBuilder>().Named("basic");
            this.Kernel.Bind<IMenuBuilder>().To<CompareMenuBuilder>().Named( "compare" );
            this.Kernel.Bind<IMenuBuilder>().To<MenuBuilderBasicViewOnly>().Named( "basic_view" );

            this.Kernel.Bind<IVirtualKeyboardInputService> ( ).To<VirtualNumericKeyboardInputService> ( ).InSingletonScope().Named ( "numeric" );
            this.Kernel.Bind<IVirtualKeyboardInputService>().To<VirtualAlphathetKeyboardInputService>().InSingletonScope().Named("alphathet");

            this.Kernel.Bind<IVirtualKeyboardInputServiceFabric> ( ).To<CommonVirtualKeyboardInputServiceFabric> ( );

            this.Kernel.Bind<IWindowHeaverService> ( ).To<CommonWindowHeaverService> ( ).InSingletonScope();

            this.Kernel.Bind<IBinaryMapper>().To<BasciBinaryMapper>().Named( "basic" );
        }

    }
}
