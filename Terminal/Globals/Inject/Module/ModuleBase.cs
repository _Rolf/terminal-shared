﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.Globals.Inject.Module {
    public abstract class ModuleBase : Ninject.Modules.NinjectModule {

        // Cписок модулей ядра
        public static IList<ModuleBase> Modules { get; protected set; } 
            = new List<ModuleBase>();


        public ModuleBase() {
            Modules.Add(this);
        }

    }
}
