﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.Globals.Inject.Module {
    public sealed class ViewModule : ModuleBase {

        public override void Load() {
            this.Kernel.Bind<View.Startup.IStartupView>().To<View.Startup.StartupView>();
            this.Kernel.Bind<View.Main.IMainView>().To<View.Main.MainView>();

            this.Kernel
                .Bind<View.Partials.View.Operator.OperatorParameterPanelPartialView.IOperatorParameterPanelPartialView>()
                .To<View.Partials.View.Operator.OperatorParameterPanelPartialView.OperatorParameterPanelPartialView>();

            this.Kernel
                .Bind<View.Partials.View.Operator.OperatorConnectionStatePartialView.IOperatorConnectionStatePartialView>()
                .To<View.Partials.View.Operator.OperatorConnectionStatePartialView.OperatorConnectionStatePartialView>();

            this.Kernel
                .Bind<Terminal.View.Partials.View.Technologist.TechnologistControlPanelPartialView.ITechnologistControlPanelPartialView>()
                .To<Terminal.View.Partials.View.Technologist.TechnologistControlPanelPartialView.TechnologistControlPanelPartialView>();

            this.Kernel
                .Bind<View.Partials.View.Technologist.TechnologistParameterPanelPartialView.ITechnologistParameterPanelPartialView>()
                .To<View.Partials.View.Technologist.TechnologistParameterPanelPartialView.TechnologistParameterPanelPartialView>();

            this.Kernel
                .Bind<View.Partials.View.OperationsManage.OperationsManageControlPartialView.IOperationsManageControlPartialView>()
                .To<View.Partials.View.OperationsManage.OperationsManageControlPartialView.OperationsManageControlPartialView>();

            this.Kernel
                .Bind<View.Partials.View.OperationsManage.OperationsManageParametersPartialView.IOperationsManageParametersPartialView>()
                .To<View.Partials.View.OperationsManage.OperationsManageParametersPartialView.OperationsManageParametersPartialView>();
        }

    }
}
