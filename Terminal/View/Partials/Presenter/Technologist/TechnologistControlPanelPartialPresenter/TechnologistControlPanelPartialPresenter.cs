﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Globals.Operations.OperationsControlService;
using Terminal.View.Partials.View.Technologist.TechnologistControlPanelPartialView;

namespace Terminal.View.Partials.Presenter.Technologist.TechnologistControlPanelPartialPresenter {
    public class TechnologistControlPanelPartialPresenter : PartialPresenterBase {

        public override string name => "technologist_control";

        public ITechnologistControlPanelPartialView view { get; set; }

        public override void Start() {
            view = _view as ITechnologistControlPanelPartialView;

            view.OperationSelected += (operation) => {
                Parent.Request("Technologist_OperationSelected", new object[] { operation });
            };

            view.OperationSave += () => {
                Parent.Request("Technologist_OperationSave", new object[] { });
            };

            view.OperationReset += () => {
                Parent.Request( "Technologist_OperationReset", new object[] { } );
            };

            var _opService = Globals.Inject.InjectService.InjectService.instance.Get<IOperationsControlService>();

            view.OperationList = _opService.Operations;
            view._Display();
        }

        public override void Stop() {}

    }
}
