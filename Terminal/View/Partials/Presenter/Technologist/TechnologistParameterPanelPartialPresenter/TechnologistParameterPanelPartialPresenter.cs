﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Terminal.Globals.Operations.OperationsControlService;
using Terminal.View.Partials.View.Technologist.TechnologistParameterPanelPartialView;
using Terminal.View.Service.MenuBuilder;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;

namespace Terminal.View.Partials.Presenter.Technologist.TechnologistParameterPanelPartialPresenter {
    public class TechnologistParameterPanelPartialPresenter : PartialPresenterBase {

        public override string name { get { return "technologist_parameter"; } }


        public ITechnologistParameterPanelPartialView view { get; set; } 

        public TechnologistDataModel _model { get; set; }
        public TechnologistDataModel _modelBackup { get; set; }

        public IMenuBuilder _menuBuilder;

        public override void Start() {
            view = _view as ITechnologistParameterPanelPartialView;

            _menuBuilder = Globals.Inject.InjectService.InjectService.instance.Get<Service.MenuBuilder.IMenuBuilder>("compare");
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Instanced compare menu!");

            view._Display();
        }

        public override void Stop() {
        }

        public void Technologist_OperationSelected(TechnologistDataModel operation) {
            SetModel(operation);
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Instaled " + operation.Name + " operation!");
        }

        public void Technologist_OperationSave() {
            var opControl = Globals.Inject.InjectService.InjectService.instance.Get<IOperationsControlService>();
            var state = opControl.SaveExistingOperation(_model);
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Saved " + _model.Name + " operation!");
            if (state.HasError) {
                MessageBox.Show(state.ToString());
                LogManager.GetCurrentClassLogger().Log(LogLevel.Error, "Saved " + _model.Name + " operation error!");
            } else {
                MessageBox.Show("Успешно сохранено!");
                LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Saved " + _model.Name + " operation successful!");
            }
        }

        public void Technologist_OperationReset() {
            _model = new TechnologistDataModel( _modelBackup );
            SetModel( _model );
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Set backup " + _modelBackup.Name + " operation!");
        }

        public void SetModel(TechnologistDataModel model) {
            _model = model;
            _modelBackup = new TechnologistDataModel( _model );

            _menuBuilder.Redraw = new Action(() => {
                view.ParametersTargetPanel.Children.Clear();
                _menuBuilder.BuildMenu( new object[] {_model, _modelBackup}, _model.GetType().GetProperties(), _model.GetType().GetFields(), view.ParametersTargetPanel);
                view.Update?.Invoke();
            });
            view.ParametersTargetPanel.Children.Clear();
            _menuBuilder.BuildMenu( new object[] { _model, _modelBackup }, _model.GetType().GetProperties(), _model.GetType().GetFields(), view.ParametersTargetPanel);
            view.Update?.Invoke();
        }

    }
}
