﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.View.Partials.Presenter {
    /// <summary>
    /// Адаптер представления родительского контроллера
    /// </summary>
    public class RequestInterfaceAdapter {

        public object _target { get; set; }

        public RequestInterfaceAdapter(object target) {
            this._target = target;
        }

        public object Request(string reqName, object[] data = null) {
            var method = _target.GetType().GetMethod( reqName );

            if (null != method)
                return method.Invoke( _target, data );
            else {
                var prop = _target.GetType().GetProperty( reqName );
                if(null != prop){
                    return prop.GetValue( _target );
                }
            }
            return null;
        }

        public object Request<T>(string reqName, object[] data = null) 
            where T : class
            {
            var resp = Request( reqName, data );
            if (null != resp)
                return resp as T;
            else return null;
        }

    }
}
