﻿using Ninject;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Globals.Operations.OperationsControlService;
using Terminal.View.Partials.View.Operator.OperatorParameterPanelPartialView;
using Terminal.View.Service.MenuBuilder;
using TerminalSharedDataModels.DataModel.WorkProccess.Operator;

namespace Terminal.View.Partials.Presenter.Operator.OperatorParameterPanelPartialPresenter {
    public class OperatorParameterPanelPartialPresenter : PartialPresenterBase {

        public override string name => "operator_parameters";

        protected IOperatorParameterPanelPartialView view { get; set; }
        protected OperatorDataModel _model { get; set; }

        [Inject] IMenuBuilder _menuBuilder { get; set; }

        public override void Start() {
            view = _view as IOperatorParameterPanelPartialView;
            _model = new OperatorDataModel();

            _menuBuilder = Globals.Inject.InjectService.InjectService.instance.Get<IMenuBuilder>("basic");
            LogManager.GetCurrentClassLogger().Log(LogLevel.Info, "Instanced basic menu!");

            _menuBuilder.Redraw += () => {
                view.ParameterPanelHeaver.Children.Clear();
                _menuBuilder.BuildMenu( new object[] { _model }, _model.GetType().GetProperties(), _model.GetType().GetFields(), view.ParameterPanelHeaver );
            };
            _menuBuilder.BuildMenu( new object[] { _model }, _model.GetType().GetProperties(), _model.GetType().GetFields() , view.ParameterPanelHeaver );

            view._Display();
        }

        public override void Stop() {}

    }
}
