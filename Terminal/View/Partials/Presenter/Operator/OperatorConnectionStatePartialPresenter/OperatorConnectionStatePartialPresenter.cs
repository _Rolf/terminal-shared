﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.View.Partials.View.Operator.OperatorConnectionStatePartialView;
using Terminal.View.Service.MenuBuilder;

namespace Terminal.View.Partials.Presenter.Operator.OperatorConnectionStatePartialPresenter {

    public class OperatorConnectionStatePartialPresenter : PartialPresenterBase {

        public override string name => "operator_connection";

        public IOperatorConnectionStatePartialView view { get; set; }

        public override void Start() {
            view = _view as IOperatorConnectionStatePartialView;

            var state = (new MotherboardStateServiceClient()).GetState();

            var _builder = Globals.Inject.InjectService.InjectService.instance.Get<IMenuBuilder>( "basic_view" );
            _builder.BuildMenu( new object[] { state }, state.GetType().GetProperties(), state.GetType().GetFields(), view.ConnectionStateMenuPanel );

            view._Display();
        }

        public override void Stop() {
        }

    }
}
