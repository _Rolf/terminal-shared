﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Terminal.Globals.Operations.OperationsControlService;
using Terminal.View.Partials.View.OperationsManage.OperationsManageParametersPartialView;
using Terminal.View.Service.MenuBuilder;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;

namespace Terminal.View.Partials.Presenter.OperationsManage.OperationsManageParametersPartialPresenter {
    public class OperationsManageParametersPartialPresenter : PartialPresenterBase {

        public override string name { get { return "OperationsManage_parameters"; } }


        public IOperationsManageParametersPartialView view { get; set; }

        private TechnologistDataModel _model { get; set; } = new TechnologistDataModel();

        public IOperationsControlService _opControl { get; set; }
        public IMenuBuilder _menuBuilder { get; set; }


        public OperationsManageParametersPartialPresenter() {}

        public override void Start() {
            view = _view as IOperationsManageParametersPartialView;

            _menuBuilder = Globals.Inject.InjectService.InjectService.instance.Get<IMenuBuilder>("basic");
            _opControl = Globals.Inject.InjectService.InjectService.instance.Get<IOperationsControlService>();

            _menuBuilder.Redraw += () => {
                view.ParametersMenuTarget.Children.Clear();
                _menuBuilder.BuildMenu(new object[] { _model }, _model.GetType().GetProperties(), _model.GetType().GetFields(), view.ParametersMenuTarget, 0, true);
                view.Update?.Invoke();
            };

            _menuBuilder.BuildMenu(new object[] { _model }, _model.GetType().GetProperties(), _model.GetType().GetFields(), view.ParametersMenuTarget,0,true);
            view.Update?.Invoke();

            view._Display();
        }

        public void OperationsControl_SaveOperation() {
            var state = _opControl.SaveNewOperation( _model );
            if (!state.HasError) {
                MessageBox.Show( "Сохранение прошло успешно" );
            } else {
                MessageBox.Show( state.ToString() );
            }
        }

        public override void Stop() {
        }

    }
}
