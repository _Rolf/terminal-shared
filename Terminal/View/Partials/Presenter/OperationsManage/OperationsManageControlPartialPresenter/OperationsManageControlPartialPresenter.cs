﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.View.Partials.View.OperationsManage.OperationsManageControlPartialView;

namespace Terminal.View.Partials.Presenter.OperationsManage.OperationsManageControlPartialPresenter {


    public class OperationsManageControlPartialPresenter : PartialPresenterBase {

        public override string name { get { return "OperationsManage_control"; } }

        public IOperationsManageControlPartialView view { get; set; }

        public override void Start() {
            view = _view as IOperationsManageControlPartialView;

            view.SaveOperation += () => {
                Parent.Request( "OperationsControl_SaveOperation", new object[] { } );
            };

            view._Display();
        }

        public override void Stop() {
        }

    }
}
