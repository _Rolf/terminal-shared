﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Terminal.Presenter;

namespace Terminal.View.Partials.Presenter {
    public abstract class PartialPresenterBase: IPresenter {

        public virtual string name { get { return "presenter"; } }

        public abstract void Start();

        public abstract void Stop();

        public RequestInterfaceAdapter asRequestAdapter { get { return new RequestInterfaceAdapter(this); } }

        private RequestInterfaceAdapter _parent;
        public RequestInterfaceAdapter Parent {
            get {
                return _parent;
            } set {
                PropertyInfo prop;

                _parent = value;

                if (_parent != null) {
                    prop = _parent._target.GetType().GetProperty( "Children" );
                    if (prop != null) {
                        try {
                            var dict = prop.GetValue( _parent._target );
                            var mt = dict.GetType().GetMethod( "Remove" );
                            mt.Invoke( dict, new object[] { name } );
                        } catch (Exception ex) {}
                    }
                }


                prop = _parent._target.GetType().GetProperty( "Children" );
                if (prop != null) {
                    try {
                        var dict = prop.GetValue( _parent._target );
                        var mt = dict.GetType().GetMethod( "Add" );
                        mt.Invoke( dict, new object[] { name , this } );
                    } catch (Exception ex) {}
                }

            } }

        public IView _view { get; set; }


    }
}
