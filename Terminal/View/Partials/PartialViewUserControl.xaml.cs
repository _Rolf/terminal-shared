﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Terminal.Presenter;

namespace Terminal.View.Partials {
    /// <summary>
    /// Логика взаимодействия для PartialViewUserControl.xaml
    /// </summary>
    public partial class PartialViewUserControl : UserControl {

        public string Capture { get; set; } = "";
        public string PartialViewType { get; set; } = "";
        public string PartialPresenterType { get; set; } = "";

        public Func<object> GetParentPresenter { get; set; }

        public PartialViewUserControl() {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e) {
            lb_Capture.Content = Capture;
            try {

                var _partialPresenterType = System.Type.GetType( PartialPresenterType, false, true );
                var partialPresenter = Globals.Inject.InjectService.InjectService.instance.Get( _partialPresenterType ) as Presenter.PartialPresenterBase;

                partialPresenter.Parent = new Presenter.RequestInterfaceAdapter( GetParentPresenter?.Invoke() );

                var _partialViewType = System.Type.GetType( PartialViewType, false, true );
                var partialView = Globals.Inject.InjectService.InjectService.instance.Get( _partialViewType ) as UIElement;

                partialPresenter._view = partialView as IView;
                partialPresenter.Start();

                fm_name.Content = partialView;

                (partialView as IView).Update += () => {
                    fm_name.Content = partialView;
                };

            } catch (Exception ex) {

                var lb = new Label() {
                    Foreground = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x00, 0x00)),
                    Content = ex.ToString()
                };

                fm_name.Content = lb;

            }
        }

    }
}
