﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.View.Partials.View.OperationsManage.OperationsManageControlPartialView {
    public interface IOperationsManageControlPartialView : IView {

        Action SaveOperation { get; set; }

    }
}
