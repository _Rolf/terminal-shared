﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Terminal.View.Partials.View.OperationsManage.OperationsManageControlPartialView {
    /// <summary>
    /// Interaction logic for OperationsManageControlPartialView.xaml
    /// </summary>
    public partial class OperationsManageControlPartialView : Page , IOperationsManageControlPartialView {
        public OperationsManageControlPartialView() {
            InitializeComponent();
        }

        public Action Update { get; set; }
        public Action SaveOperation { get; set; }

        public void _Display() {
        }

        public void _Hide() {
        }

        private void bt_SaveOperation_Click(object sender, RoutedEventArgs e) {
            SaveOperation?.Invoke();
        }
    }
}
