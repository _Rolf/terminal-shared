﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Terminal.View.Partials.View.OperationsManage.OperationsManageParametersPartialView {
    public interface IOperationsManageParametersPartialView : IView {

        Panel ParametersMenuTarget { get;} 

    }
}
