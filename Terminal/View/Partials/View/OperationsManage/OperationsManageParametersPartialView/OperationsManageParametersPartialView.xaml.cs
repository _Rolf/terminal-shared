﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Terminal.View.Partials.View.OperationsManage.OperationsManageParametersPartialView {
    /// <summary>
    /// Interaction logic for OperationsManageParametersPartialView.xaml
    /// </summary>
    public partial class OperationsManageParametersPartialView : Page , IOperationsManageParametersPartialView {
        public OperationsManageParametersPartialView() {
            InitializeComponent();
        }

        public Panel ParametersMenuTarget {
            get {
                return sp_Parameters;
            }
        }

        public Action Update { get; set; }

        public void _Display() {
        }

        public void _Hide() {
        }


    }
}
