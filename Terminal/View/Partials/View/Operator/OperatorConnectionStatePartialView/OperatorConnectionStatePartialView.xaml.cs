﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Terminal.View.Partials.View.Operator.OperatorConnectionStatePartialView {
    /// <summary>
    /// Логика взаимодействия для OperatorConnectionStatePartialView.xaml
    /// </summary>
    public partial class OperatorConnectionStatePartialView : Page, IOperatorConnectionStatePartialView {
        public OperatorConnectionStatePartialView() {
            InitializeComponent();
        }

        public Action Update { get; set; }

        public Panel ConnectionStateMenuPanel  { get {
                return sp_MenuMain;
            }
        }

        public void _Display() {}
        public void _Hide() {}

    }
}
