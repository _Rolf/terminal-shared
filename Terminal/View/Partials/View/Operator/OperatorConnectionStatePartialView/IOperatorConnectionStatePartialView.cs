﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Terminal.View.Partials.View.Operator.OperatorConnectionStatePartialView {

    public interface IOperatorConnectionStatePartialView : IView {

        Panel ConnectionStateMenuPanel { get; }

    }

}
