﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;

namespace Terminal.View.Partials.View.Operator.OperatorParameterPanelPartialView {
    /// <summary>
    /// Логика взаимодействия для OperatorParameterPanelPartialView.xaml
    /// </summary>
    public partial class OperatorParameterPanelPartialView : Page , IOperatorParameterPanelPartialView  {
        public OperatorParameterPanelPartialView() {
            InitializeComponent();
        }

        public Panel ParameterPanelHeaver => sp_main;

        public Action Update { get; set; }

        public void _Display() {
        }

        public void _Hide() {
        }

    }
}
