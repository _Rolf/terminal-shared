﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;

namespace Terminal.View.Partials.View.Operator.OperatorParameterPanelPartialView {
    public interface IOperatorParameterPanelPartialView : IView {

        System.Windows.Controls.Panel ParameterPanelHeaver { get; }

    }
}
