﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;

namespace Terminal.View.Partials.View.Technologist.TechnologistControlPanelPartialView {
    public interface ITechnologistControlPanelPartialView : IView {

        List<TechnologistDataModel> OperationList { set; }

        Action<TechnologistDataModel> OperationSelected { get; set; }

        Action OperationSave { get; set; }
        Action OperationReset { get; set; }

    }
}
