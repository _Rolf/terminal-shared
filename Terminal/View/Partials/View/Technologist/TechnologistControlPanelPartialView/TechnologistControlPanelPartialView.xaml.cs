﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;

namespace Terminal.View.Partials.View.Technologist.TechnologistControlPanelPartialView {
    /// <summary>
    /// Логика взаимодействия для TechnologistControlPanelPartialView.xaml
    /// </summary>
    public partial class TechnologistControlPanelPartialView : Page , ITechnologistControlPanelPartialView {
        public TechnologistControlPanelPartialView() {
            InitializeComponent();
        }


        private List<TechnologistDataModel> OperationListCache = new List<TechnologistDataModel>();
        public List<TechnologistDataModel> OperationList {
            set {
                OperationListCache = value;
                foreach (var op in value) {
                    cb_OperationSelect.Items.Add( op.Name );
                }
            }
        }

        public Action Update { get; set; }

        public Action<TechnologistDataModel> OperationSelected {get;set;}

        public Action OperationSave { get; set; }
        public Action OperationReset { get; set; }

        public void _Display() {
        }

        public void _Hide() {
        }

        private void cb_OperationSelect_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            OperationSelected?.Invoke(OperationListCache[cb_OperationSelect.SelectedIndex]);
        }

        private void bt_Save_Click(object sender, RoutedEventArgs e) {
            OperationSave?.Invoke();
        }

        private void bt_Cancel_Click(object sender, RoutedEventArgs e) {
            OperationReset?.Invoke();
        }
    }
}
