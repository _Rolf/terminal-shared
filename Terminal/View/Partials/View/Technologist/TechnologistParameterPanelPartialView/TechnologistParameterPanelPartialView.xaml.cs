﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Terminal.View.Partials.View.Technologist.TechnologistParameterPanelPartialView {
    /// <summary>
    /// Interaction logic for TechnologistParameterPanelPartialView.xaml
    /// </summary>
    public partial class TechnologistParameterPanelPartialView : Page , ITechnologistParameterPanelPartialView {
        public TechnologistParameterPanelPartialView() {
            InitializeComponent();
        }

        public Panel ParametersTargetPanel {
            get {
                return sp_main;
            }
        }

        public Action Update { get; set; }

        public void _Display() {
        }

        public void _Hide() {
        }

    }
}
