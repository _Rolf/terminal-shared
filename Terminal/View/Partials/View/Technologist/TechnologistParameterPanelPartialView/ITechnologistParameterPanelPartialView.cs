﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Terminal.View.Partials.View.Technologist.TechnologistParameterPanelPartialView {
    public interface ITechnologistParameterPanelPartialView : IView{

        Panel ParametersTargetPanel { get; }

    }
}
