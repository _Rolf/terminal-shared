﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.View.Main {
    public interface IMainView : IView {

        Func<object> GetPresenter { get; set; }


    }
}
