﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Terminal.Globals.Configuration;
using Terminal.Globals.Configuration.DataModel;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;

namespace Terminal.View.Main {
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window , IMainView {

        [Inject] public IConfigurationService _config { get; set; }

        #region Блокировка закрытия и движения окна (свистопляска с вызовами windows)

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);


        const int WM_SYSCOMMAND = 0x0112;
        const int SC_MOVE = 0xF010;

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled) {

            switch (msg) {
                case WM_SYSCOMMAND:
                    int command = wParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE) {
                        handled = true;
                    }
                    break;
                default:
                    break;
            }
            return IntPtr.Zero;
        }

        public MainView() {
            InitializeComponent();

            _config = Globals.Inject.InjectService.InjectService.instance.Get<IConfigurationService>();
            var _cfg = _config["ConfigurationMainWindow"] as ConfigurationMainWindowDataModel;

            if (_cfg.OverTopMode) {
                Loaded += MainView_Loaded;
                SourceInitialized += MainView_SourceInitialized;
            }

        }

        private void MainView_SourceInitialized(object sender, EventArgs e) {
            WindowInteropHelper helper = new WindowInteropHelper(this);
            HwndSource source = HwndSource.FromHwnd(helper.Handle);
            source.AddHook(WndProc);
        }

        private void MainView_Loaded(object sender, RoutedEventArgs e) {
            var hwnd = new System.Windows.Interop.WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }
        #endregion



        public Panel OperatorPropertiesBuildingTargetPanel {
            get {
                return null ;
            }
        }

        public Func<object> GetPresenter { get; set; }

        public Action<TechnologistDataModel> OperationSelected { get; set; }

        public void _Display() {
            Show();
        }

        public void _Hide() {
            Close();
        }

        public Action Update { get; set; }

        private void Window_Initialized(object sender, EventArgs e) {
            pv_OperatorControls.GetParentPresenter = new Func<object>(() => GetPresenter.Invoke());
            pv_OperatorInfo.GetParentPresenter = new Func<object>( () => GetPresenter.Invoke() );
            pv_TechnologistControl.GetParentPresenter = new Func<object>( () => GetPresenter.Invoke() );
            pv_TechnologistProperties.GetParentPresenter = new Func<object>(() => GetPresenter.Invoke());
            pv_OperationsManage_Control.GetParentPresenter = new Func<object>(() => GetPresenter.Invoke());
            pv_OperationsManage_Parameters.GetParentPresenter = new Func<object>(() => GetPresenter.Invoke());
        }



        private void bt_OperationsManage_Scroll_Top_Click(object sender, RoutedEventArgs e) {
            if(sv_OperationsManage_Parameters.VerticalOffset < sv_OperationsManage_Parameters.ScrollableHeight)
            sv_OperationsManage_Parameters.ScrollToVerticalOffset( sv_OperationsManage_Parameters.VerticalOffset + 70 );
        }

        private void bt_OperationsManage_Scroll_Bottom_Click(object sender, RoutedEventArgs e) {
            if (sv_OperationsManage_Parameters.VerticalOffset > 0)
                sv_OperationsManage_Parameters.ScrollToVerticalOffset( sv_OperationsManage_Parameters.VerticalOffset - 70 );
        }

        private void bt_Technologist_Scroll_Top_Click ( object sender, RoutedEventArgs e ) {
            if ( sv_Technologist_Parameters.VerticalOffset < sv_Technologist_Parameters.ScrollableHeight )
                sv_Technologist_Parameters.ScrollToVerticalOffset ( sv_Technologist_Parameters.VerticalOffset + 70 );
        }

        private void bt_Technologist_Scroll_Bottom_Click ( object sender, RoutedEventArgs e ) {
            if ( sv_Technologist_Parameters.VerticalOffset > 0 )
                sv_Technologist_Parameters.ScrollToVerticalOffset ( sv_Technologist_Parameters.VerticalOffset - 70 );
        }

    }
}
