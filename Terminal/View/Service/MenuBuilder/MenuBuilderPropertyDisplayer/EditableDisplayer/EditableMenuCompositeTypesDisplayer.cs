﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer {
    public class EditableMenuCompositeTypesDisplayer : MenuBuilderPropertyDisplayerBase {

        public Dictionary<string, bool> DropDownStates = new Dictionary<string, bool>();

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {
            var target = targets[0];

            if(!DropDownStates.ContainsKey( target.GetHashCode().ToString() + target.GetType().Name + pinfo.Name ))
                DropDownStates.Add( target.GetHashCode().ToString() + target.GetType().Name + pinfo.Name, false );

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Vertical,
                Margin = new Thickness( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };

            var bd = new Border() {
                BorderBrush = GetPropertyDisplayBrush( pinfo ),
                BorderThickness = new Thickness( 2, 2, 2, 2 ),
                Width = _buildConfig.ListViewWidth,
                HorizontalAlignment = HorizontalAlignment.Left
            };

            var sp = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Vertical
            };

            var opCh = DropDownStates[target.GetHashCode().ToString() + target.GetType().Name + pinfo.Name] ? "-" : "+" ;

            var lbCapture = new Button() {
                Content = opCh + "  " + GetPropertyDisplayName( pinfo ),
                Background = GetPropertyDisplayBrush(pinfo),
                FontSize = _buildConfig.FontSize,
                HorizontalAlignment = HorizontalAlignment.Left,
                HorizontalContentAlignment = HorizontalAlignment.Left,
                Width = _buildConfig.ListViewWidth,
                BorderThickness = new Thickness( 0, 0, 0, 0 )
            };

            lbCapture.Click += (o, e) => {
                DropDownStates[target.GetHashCode().ToString() + target.GetType().Name + pinfo.Name] =
                    !DropDownStates[target.GetHashCode().ToString() + target.GetType().Name + pinfo.Name];
                CallRedrawCallback?.Invoke();
            };

            var objtarget = pinfo.GetValue( target );

            BuildMenuCallback?.Invoke(
                new object[] { objtarget },
                objtarget.GetType().GetProperties(),
                objtarget.GetType().GetFields(),
                sp,
                left + _buildConfig.LeftStep,
                IgnoreHide
            );

            if (DropDownStates[target.GetHashCode().ToString() + target.GetType().Name + pinfo.Name]) {
                bd.Child = sp;
                spMain.Children.Add( lbCapture );
                spMain.Children.Add( bd );
            } else {
                spMain.Children.Add( lbCapture );
            }


            return spMain;
        }

    }
}
