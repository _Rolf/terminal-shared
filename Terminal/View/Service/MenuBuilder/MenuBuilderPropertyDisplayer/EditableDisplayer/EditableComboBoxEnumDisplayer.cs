﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer {
    public class EditableComboBoxEnumDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {
            var target = targets[0];

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };

            var enumvalues = Enum.GetValues( pinfo.PropertyType );
            var underlyingType = Enum.GetUnderlyingType( pinfo.PropertyType );

            var sp = new StackPanel() {
                Orientation = Orientation.Vertical,
                HorizontalAlignment = HorizontalAlignment.Stretch
            };

            var lb = new Label() {
                Content = GetPropertyDisplayName( pinfo ),
                Width = _buildConfig.LabelWidth - left,
                HorizontalAlignment = HorizontalAlignment.Left,
                FontSize = _buildConfig.FontSize
            };

            var cb = new ComboBox() {
                Width = _buildConfig.ComboBoxWidth - left
            };

            sp.Children.Add( lb );
            sp.Children.Add( cb );

            spMain.Children.Add( sp );

            var index = 0;
            var currentval = pinfo.GetValue( target );
            foreach (var value in enumvalues) {
                cb.Items.Add( value.ToString() );
                if (currentval.Equals( value ))
                    cb.SelectedIndex = index;
                index++;
            }

            return spMain;
        }


    }
}
