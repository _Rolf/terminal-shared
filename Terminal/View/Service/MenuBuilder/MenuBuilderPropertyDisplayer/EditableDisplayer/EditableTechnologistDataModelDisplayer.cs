﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Terminal.Globals.Operations.OperationsControlService;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer {
    public class EditableTechnologistDataModelDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {
            var target = targets[0];

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };


            var spVert = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Vertical
            };

            var lbName = new Label() {
                Content = GetPropertyDisplayName( pinfo ),
                FontSize = _buildConfig.FontSize
            };


            var _opController = Globals.Inject.InjectService.InjectService.instance.Get<IOperationsControlService>();

            var lOperations = _opController.Operations;

            var cbMain = new ComboBox() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                FontSize = _buildConfig.FontSize,
                MinWidth = _buildConfig.ComboBoxWidth
            };

            foreach (var op in lOperations) {
                var _op = op;
                cbMain.Items.Add( _op.Name );
            }

            cbMain.SelectionChanged += (o, e) => {
                var selOp = lOperations[cbMain.SelectedIndex];
                pinfo.SetValue( target, selOp );
            };

            spVert.Children.Add( lbName );
            spVert.Children.Add( cbMain );

            spMain.Children.Add( spVert );

            return spMain;
        }

    }
}
