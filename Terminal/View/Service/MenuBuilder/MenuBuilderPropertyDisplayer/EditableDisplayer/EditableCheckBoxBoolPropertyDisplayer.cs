﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer {
    public class EditableCheckBoxBoolPropertyDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {
            var target = targets[0];

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };

            var lb = new Label() {
                Content = GetPropertyDisplayName( pinfo ),
                Width = _buildConfig.LabelWidth - left,
                FontSize = _buildConfig.FontSize
            };

            var val = (bool)pinfo.GetValue( target );


            var cb_val = new CheckBox();
            cb_val.IsChecked = val;

            cb_val.Checked += (o, e) => {
                pinfo.SetValue( target, cb_val.IsChecked );
            };

            spMain.Children.Add( lb );
            spMain.Children.Add( cb_val );

            return spMain;

        }

    }
}
