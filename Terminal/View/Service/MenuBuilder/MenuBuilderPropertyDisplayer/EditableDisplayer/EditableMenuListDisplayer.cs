﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer {
    public class EditableMenuListDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {
            var target = targets[0];

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };

            var listElementType = pinfo.PropertyType.GenericTypeArguments[0];

            var sp = new StackPanel() {
                Orientation = Orientation.Vertical,
                HorizontalAlignment = HorizontalAlignment.Stretch,
            };

            var spItems = new StackPanel() {
                Orientation = Orientation.Vertical,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xEE, 0xEE, 0xEE ) ),
            };

            var lb = new Label() {
                Content = GetPropertyDisplayName( pinfo ),
                Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xD5, 0xD5, 0xD6 ) ),
                Width = _buildConfig.ListViewWidth - left,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                FontSize = _buildConfig.FontSize
            };

            var cntprop = pinfo.PropertyType.GetProperty( "Count" );
            var mlist = pinfo.GetValue( target );

            if (mlist != null) {
                var cnt = (int)cntprop.GetValue( mlist );

                var lbCnt = new Label() {
                    Content = "Элементов: " + cnt.ToString(),
                    Width = _buildConfig.ListViewWidth - left,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    FontSize = _buildConfig.FontSize
                };

                var btAdd = new Button() {
                    Content = "+",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Width = _buildConfig.ListViewWidth - left,
                    FontSize = _buildConfig.FontSize
                };

                btAdd.Click += (o, e) => {
                    var element = Activator.CreateInstance( listElementType );
                    var addMt = mlist.GetType().GetMethod( "Add" ).Invoke( mlist, new object[] { element } );
                    CallRedrawCallback?.Invoke();
                };

                for (var i = 0; i < cnt; i++) {
                    var index = i;
                    var item = mlist.GetType().GetProperty( "Item" ).GetValue( mlist, new object[] { i } );

                    var spItem = new StackPanel() {
                        Orientation = Orientation.Vertical,
                        HorizontalAlignment = HorizontalAlignment.Stretch
                    };

                    var spCapture = new StackPanel() {
                        Orientation = Orientation.Horizontal,
                        HorizontalAlignment = HorizontalAlignment.Stretch
                    };

                    var lbItemCapture = new Label() {
                        Content = "Элемент  " + i.ToString(),
                        Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xD5, 0xD5, 0xD6 ) ),
                        Width = _buildConfig.ListViewWidth - 40 - left,
                        FontSize = _buildConfig.FontSize
                    };

                    var btRemove = new Button() {
                        Content = "-",
                        Width = 40
                    };


                    btRemove.Click += (o, e) => {
                        var ii = index;
                        var rmMet = mlist.GetType().GetMethod( "RemoveAt" ).Invoke( mlist, new object[] { ii } );
                        CallRedrawCallback.Invoke();
                    };

                    spCapture.Children.Add( lbItemCapture );
                    spCapture.Children.Add( btRemove );

                    spItems.Children.Add( spCapture );

                    BuildMenuCallback?.Invoke(
                        new object[] { item },
                        item.GetType().GetProperties(),
                        item.GetType().GetFields(),
                        spItem,
                        left + _buildConfig.LeftStep,
                        IgnoreHide
                        );

                    spItems.Children.Add( spItem );

                }

                sp.Children.Add( lb );
                sp.Children.Add( lbCnt );
                sp.Children.Add( btAdd );

                sp.Children.Add( spItems );


                spMain.Children.Add( sp );

                return spMain;

            } else return null;
        }


    }
}
