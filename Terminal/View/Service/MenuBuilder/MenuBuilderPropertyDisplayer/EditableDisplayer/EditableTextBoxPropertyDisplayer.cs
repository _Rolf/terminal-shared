﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Terminal.View.Service.VirtualKeyboardInputService.Factory;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer {
    public class EditableTextBoxPropertyDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {
            var target = targets[0];

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };

            var lb = new System.Windows.Controls.Label() {
                Content = GetPropertyDisplayName( pinfo ),
                Width = _buildConfig.LabelWidth - left,
                FontSize = _buildConfig.FontSize
            };

            var tb = new TextBox() {
                MinWidth = _buildConfig.TextBoxMinWidth - left,
                FontSize = _buildConfig.FontSize
            };

            var readonlyAttr = Attribute.GetCustomAttribute( pinfo, typeof( ReadOnlyAttribute ) ) as ReadOnlyAttribute;

            if (null != readonlyAttr && !IgnoreHide) {
                tb.IsEnabled = false;
            }

            spMain.Children.Add( lb );
            spMain.Children.Add( tb );

            var val = pinfo.GetValue( target, null );
            if (null != val)
                tb.Text = val.ToString();

            tb.TextChanged += (o, e) => {
                var typeConverter = System.ComponentModel.TypeDescriptor.GetConverter( pinfo.PropertyType );
                if (typeConverter.CanConvertFrom( typeof( string ) )) {
                    try {
                        pinfo.SetValue( target, typeConverter.ConvertFrom( tb.Text ) );
                        tb.Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xFF, 0xFF, 0xFF ) );
                    } catch (Exception) {
                        tb.Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xE7, 0x4C, 0x3C ) );
                    }
                }
            };

            tb.GotFocus += ( o, e ) => {
                var factory = Globals.Inject.InjectService.InjectService.instance.Get<IVirtualKeyboardInputServiceFabric> ( );
                factory.GenerateService ( pinfo.PropertyType ).ShowAt ( tb );
            };

            return spMain;
        }

    }
}
