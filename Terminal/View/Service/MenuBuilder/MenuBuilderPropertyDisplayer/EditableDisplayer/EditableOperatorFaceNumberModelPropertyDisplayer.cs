﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TerminalSharedDataModels.DataModel.WorkProccess.Operator;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer {
    public class EditableOperatorFaceNumberModelPropertyDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty ( object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false ) {

            var target = targets[0];

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel ( ) {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness ( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };


            var model = pinfo.GetValue(target) as OperatorFaceNumberModel;

            var spVert = new StackPanel ( ) {
                Orientation = Orientation.Vertical,
                Margin = new Thickness ( 0, 0, 0, 0 ),
            };


            var lbDesc = new Label ( ) {
                Content = GetPropertyDisplayName ( pinfo ),

                FontSize = _buildConfig.FontSize,
                MinWidth = _buildConfig.LabelWidth,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Background = GetPropertyDisplayBrush ( pinfo ),
                Width = _buildConfig.ListViewWidth
            };

            var spHor = new StackPanel ( ) {
                Orientation = Orientation.Horizontal,
                HorizontalAlignment = HorizontalAlignment.Stretch
            };


            var bt1 = new Button ( ) {
                Content = "1",
                Width = _buildConfig.ListViewWidth/2,
                FontSize = _buildConfig.FontSize
            };

            var bt2 = new Button ( ) {
                Content = "2",
                Width = _buildConfig.ListViewWidth / 2,
                FontSize = _buildConfig.FontSize
            };

            bt1.Background = new SolidColorBrush ( Color.FromArgb ( 0xFF, 0x95, 0xA5, 0xA6 ) );
            bt2.Background = new SolidColorBrush ( Color.FromArgb ( 0xFF, 0x95, 0xA5, 0xA6 ) );

            bt1.Background = new SolidColorBrush ( Color.FromArgb ( 0xFF, 0x2E, 0xCC, 0x71 ) );

            bt1.Click += ( o, e ) => {
                bt1.Background = new SolidColorBrush ( Color.FromArgb ( 0xFF, 0x2E, 0xCC, 0x71 ) );
                bt2.Background = new SolidColorBrush ( Color.FromArgb ( 0xFF, 0x95, 0xA5, 0xA6 ) );
                model.number = 1;
            };

            bt2.Click += ( o, e ) => {
                bt1.Background = new SolidColorBrush ( Color.FromArgb ( 0xFF, 0x95, 0xA5, 0xA6 ) );
                bt2.Background = new SolidColorBrush ( Color.FromArgb ( 0xFF, 0x2E, 0xCC, 0x71 ) );
                model.number = 2;
            };


            spHor.Children.Add ( bt1 );
            spHor.Children.Add ( bt2 );

            spVert.Children.Add ( lbDesc );

            spVert.Children.Add ( spHor );
            spMain.Children.Add ( spVert );

            return spMain;
        }

    }
}
