﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Terminal.View.Service.VirtualKeyboardInputService;
using TerminalSharedDataModels.DataModel.Wrapper;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer {
    public class DecimalMeasureWrapperEditablePropertyDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {
            var target = targets[0];

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };

            var spVert = new StackPanel() {
                Orientation = Orientation.Vertical,
                Margin = new Thickness(0,0,0,0)
            };

            var wrapper = pinfo.GetValue(target,null) as DecimalMeasureWrapper;


            var lbDesc = new Label() {
                Content = GetPropertyDisplayName( pinfo ) ,
          
                FontSize = _buildConfig.FontSize,
                MinWidth = _buildConfig.LabelWidth,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Background = GetPropertyDisplayBrush(pinfo),
                Width = _buildConfig.ListViewWidth
            };

            var spHor = new StackPanel() {
                Orientation = Orientation.Horizontal
            };


            var lbLimits = new Label() {
                Content = wrapper.measureUnit + "  " + wrapper.valueMin + " - " + wrapper.valueMax + "" ,
                FontSize = _buildConfig.FontSize - 2,
                HorizontalAlignment = HorizontalAlignment.Stretch
            };


            var tb = new TextBox() {
                MinWidth = _buildConfig.TextBoxMinWidth,
                FontSize = _buildConfig.FontSize,
                Text = wrapper.value.ToString(),
            };

            tb.GotFocus += ( o, e ) => {
                var kb = Globals.Inject.InjectService.InjectService.instance.Get<IVirtualKeyboardInputService> ( "numeric" );
                kb.ShowAt ( tb );
            };

            tb.TextChanged += (o, e) => {

                try {
                    var d = decimal.Parse( tb.Text );

                    if (wrapper.isOkLimit( d )) {
                        wrapper.value = d;
                        tb.Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xFF, 0xFF, 0xFF ) );
                    } else throw new ArgumentException();

                } catch (Exception ex) {
                    tb.Background =  new SolidColorBrush(Color.FromArgb(0xFF,0xD7,0x4C,0x3C));
                }

            };


            spVert.Children.Add( lbDesc );

            spHor.Children.Add( tb );
            spHor.Children.Add( lbLimits );

            spVert.Children.Add( spHor );

            spMain.Children.Add( spVert );

            return spMain;
        }

    }
}
