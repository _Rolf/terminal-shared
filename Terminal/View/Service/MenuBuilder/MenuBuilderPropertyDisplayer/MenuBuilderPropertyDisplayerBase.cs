﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Terminal.Globals.Configuration;
using TerminalSharedDataModels.Attributes;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer {
    public abstract class MenuBuilderPropertyDisplayerBase {

        [Inject] public IConfigurationService _config { get; set; }

        public Action<object[], IList<PropertyInfo>, IList<FieldInfo>,Panel, int, bool> BuildMenuCallback { get; set; }
        public Action CallRedrawCallback { get; set; }

        public MenuBuilderPropertyDisplayerBase() {
            _config = Globals.Inject.InjectService.InjectService.instance.Get<IConfigurationService>();
        }

        public abstract UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false);

        protected string GetPropertyDisplayName(PropertyInfo pinfo) {
            var propName = pinfo.Name;
            var displaynameAttr = Attribute.GetCustomAttribute( pinfo, typeof( DisplayNameAttribute ) )
                as DisplayNameAttribute;
            if (displaynameAttr != null)
                propName = displaynameAttr.DisplayName;
            return propName;
        }

        protected virtual Brush GetPropertyDisplayBrush(PropertyInfo pinfo) {

            var Default = Color.FromArgb( 0xFF, 0xD6, 0xD6, 0xD6 );

            var attr = Attribute.GetCustomAttribute( pinfo, typeof( DisplayColorAttribute ) ) as DisplayColorAttribute;
            if (null != attr) Default =  attr.color ;

            return new SolidColorBrush(Default);
        }

    }
}
