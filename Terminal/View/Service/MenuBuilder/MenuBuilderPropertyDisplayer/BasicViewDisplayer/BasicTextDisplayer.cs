﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.BasicViewDisplayer {
    public class BasicTextDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {

            var target = targets[0];

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                Orientation = Orientation.Horizontal
            };

            var lbName = new Label() {
                Content = GetPropertyDisplayName( pinfo ),
                Width = _buildConfig.LabelWidth - left,
                FontSize = _buildConfig.FontSize
            };

            var lbVal = new Label() {
                Content = pinfo.GetValue( target, null ).ToString(),
                Width = _buildConfig.LabelWidth - left,
                FontSize = _buildConfig.FontSize
            };

            spMain.Children.Add( lbName );
            spMain.Children.Add( lbVal );

            return spMain;

        }

    }
}
