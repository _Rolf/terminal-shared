﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.CompareDisplayer {
    public class MenuListCompareDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };

            var tplTarget = new Tuple<object, object>( targets[0], targets[1] );

            var listElementType = pinfo.PropertyType.GenericTypeArguments[0];

            var sp = new StackPanel() {
                Orientation = Orientation.Vertical,
                HorizontalAlignment = HorizontalAlignment.Stretch,
            };

            var spItems = new StackPanel() {
                Orientation = Orientation.Vertical,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xEE, 0xEE, 0xEE ) ),
            };

            var lb = new Label() {
                Content = GetPropertyDisplayName( pinfo ),
                Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xD5, 0xD5, 0xD6 ) ),
                Width = _buildConfig.ListViewWidth - left,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                FontSize = _buildConfig.FontSize
            };

            var mlist = pinfo.GetValue( tplTarget.Item1 );
            var mlistComp = pinfo.GetValue( tplTarget.Item2 );

            if (mlist != null) {
                var cntprop = pinfo.PropertyType.GetProperty( "Count" );
                var cnt = (int)cntprop.GetValue( mlist );

                var lbCnt = new Label() {
                    Content = "Элементов: " + cnt.ToString(),
                    Width = _buildConfig.ListViewWidth - left,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    FontSize = _buildConfig.FontSize
                };

                var btAdd = new Button() {
                    Content = "+",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Width = _buildConfig.ListViewWidth - left,
                    FontSize = _buildConfig.FontSize
                };

                btAdd.Click += (o, e) => {
                    var element = Activator.CreateInstance( listElementType );
                    var addMt = mlist.GetType().GetMethod( "Add" ).Invoke( mlist, new object[] { element } );
                    var addMtComp = mlistComp.GetType().GetMethod( "Add" ).Invoke( mlistComp, new object[] { element } );
                    CallRedrawCallback();
                };

                for (var i = 0; i < cnt; i++) {
                    var index = i;
                    var item = mlist.GetType().GetProperty( "Item" ).GetValue( mlist, new object[] { i } );
                    var itemComp = mlistComp.GetType().GetProperty( "Item" ).GetValue( mlistComp, new object[] { i } );

                    var spItem = new StackPanel() {
                        Orientation = Orientation.Vertical,
                        HorizontalAlignment = HorizontalAlignment.Stretch
                    };

                    var spCapture = new StackPanel() {
                        Orientation = Orientation.Horizontal,
                        HorizontalAlignment = HorizontalAlignment.Stretch
                    };

                    var lbItemCapture = new Label() {
                        Content = "Элемент  " + i.ToString(),
                        Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xD5, 0xD5, 0xD6 ) ),
                        Width = _buildConfig.ListViewWidth - left,
                        FontSize = _buildConfig.FontSize
                    };

                    var btRemove = new Button() {
                        Content = "-",
                        Width = 40
                    };


                    btRemove.Click += (o, e) => {
                        var ii = index;
                        var rmMet = mlist.GetType().GetMethod( "RemoveAt" ).Invoke( mlist, new object[] { ii } );
                        var rmMetComp = mlistComp.GetType().GetMethod( "RemoveAt" ).Invoke( mlistComp, new object[] { ii } );
                        CallRedrawCallback?.Invoke();
                    };

                    spCapture.Children.Add( lbItemCapture );
                    //spCapture.Children.Add( btRemove );

                    spItems.Children.Add( spCapture );

                    BuildMenuCallback?.Invoke(
                        new object[] { item, itemComp },
                        item.GetType().GetProperties(),
                        item.GetType().GetFields(),
                        spItem,
                        left + _buildConfig.LeftStep,
                        IgnoreHide
                        );

                    spItems.Children.Add( spItem );

                }

                sp.Children.Add( lb );
                sp.Children.Add( lbCnt );
                //sp.Children.Add( btAdd );

                sp.Children.Add( spItems );


                spMain.Children.Add( sp );

                return spMain;
            } else return null;
        }

    }
}
