﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.CompareDisplayer {
    public class ComboBoxCompareDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };

            var tplTarget = new Tuple<object, object>( targets[0], targets[1] );

            var lb = new Label() {
                Content = GetPropertyDisplayName( pinfo ),
                Width = _buildConfig.LabelWidth - left,
                FontSize = _buildConfig.FontSize
            };

            var val = (bool)pinfo.GetValue( tplTarget.Item1, null );
            var cval = (bool)pinfo.GetValue( tplTarget.Item2, null );

            var cb_val = new CheckBox();
            cb_val.IsChecked = val;

            cb_val.Checked += (o, e) => {
                pinfo.SetValue( tplTarget.Item1, cb_val.IsChecked );
            };

            var cb_compare = new CheckBox();
            cb_compare.IsChecked = cval;
            cb_compare.IsEnabled = false;

            spMain.Children.Add( lb );
            spMain.Children.Add( cb_val );
            spMain.Children.Add( cb_compare );

            return spMain;

        }

    }
}
