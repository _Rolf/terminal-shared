﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer;
using Terminal.View.Service.VirtualKeyboardInputService;
using TerminalSharedDataModels.DataModel.Wrapper;

namespace Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.CompareDisplayer {
    public class CompareDecimalWrapperPropertyDisplayer : MenuBuilderPropertyDisplayerBase {

        public override UIElement DisplayProperty(object[] targets, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {

            if (targets.Length == 1) {
                return new DecimalMeasureWrapperEditablePropertyDisplayer().DisplayProperty( targets, pinfo, left, IgnoreHide );
            }

            var _buildConfig = _config["BuildMenuProperties"] as Globals.Configuration.DataModel.ConfigurationBuildMenuPropertiesDataModel;

            var spMain = new StackPanel() {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness( left, _buildConfig.SPMarginTop, 0, _buildConfig.SPMarginBottom )
            };

            var tplTarget = new Tuple<object, object>( targets[0], targets[1] );

            var spVert = new StackPanel() {
                Orientation = Orientation.Vertical
            };

            var wrapper = pinfo.GetValue( tplTarget.Item1, null ) as DecimalMeasureWrapper;
            var cwrapper = pinfo.GetValue( tplTarget.Item2, null ) as DecimalMeasureWrapper;


            var lbDesc = new Label() {
                Content = GetPropertyDisplayName( pinfo ),
                FontSize = _buildConfig.FontSize,
                Width = _buildConfig.ListViewWidth,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Background = GetPropertyDisplayBrush(pinfo)
            };


            var lbLimits = new Label() {
                Width = _buildConfig.ListViewWidth / 2,
                Content = wrapper.measureUnit + "  " + wrapper.valueMin + " - " + wrapper.valueMax + "",
                FontSize = _buildConfig.FontSize - 2,
                HorizontalAlignment = HorizontalAlignment.Stretch
            };

            var spTb = new StackPanel() {
                Orientation = Orientation.Horizontal
            };

            var tb = new TextBox() {
                Width = _buildConfig.ListViewWidth / 4,
                FontSize = _buildConfig.FontSize,
                Text = wrapper.value.ToString(),
            };

            tb.GotFocus += ( o, e ) => {
                var kb = Globals.Inject.InjectService.InjectService.instance.Get<IVirtualKeyboardInputService> ("numeric");
                kb.ShowAt ( tb );
            };

            var ctb = new TextBox() {
                MinWidth = _buildConfig.ListViewWidth / 4,
                FontSize = _buildConfig.FontSize,
                Text = cwrapper.value.ToString(),
                IsEnabled = false
            };


            spTb.Children.Add( tb );
            spTb.Children.Add( ctb );
            spTb.Children.Add( lbLimits );


            tb.TextChanged += (o, e) => {

                try {
                    var d = decimal.Parse( tb.Text );

                    if (wrapper.isOkLimit( d )) {
                        wrapper.value = d;
                        tb.Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xFF, 0xFF, 0xFF ) );
                    } else throw new ArgumentException();

                } catch (Exception ex) {
                    tb.Background = new SolidColorBrush( Color.FromArgb( 0xFF, 0xD7, 0x4C, 0x3C ) );
                }

            };


            spVert.Children.Add( lbDesc );
            spVert.Children.Add( spTb );

            spMain.Children.Add( spVert );

            return spMain;


        }

    }
}
