﻿using Ninject;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Terminal.Globals.Configuration;
using Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer;
using TerminalSharedDataModels.Attributes;
using TerminalSharedDataModels.DataModel.WorkProccess.Operator;
using TerminalSharedDataModels.DataModel.WorkProccess.Technologist;
using TerminalSharedDataModels.DataModel.Wrapper;

namespace Terminal.View.Service.MenuBuilder {
    public class BasicMenuBuilder : MenuBuilderBase {


        public BasicMenuBuilder() {

            PropertyTypeToDisplayerDict = new Dictionary<HashSet<Type>, MenuBuilderPropertyDisplayer.MenuBuilderPropertyDisplayerBase>() {
                { new HashSet<Type>() {
                    typeof(int) ,
                    typeof(uint),
                    typeof(string),
                    typeof(float),
                    typeof(double),
                    typeof(byte),
                    typeof(decimal)
                } , new EditableTextBoxPropertyDisplayer()  },
                { new HashSet<Type>(){
                    typeof(bool)
                }, new EditableCheckBoxBoolPropertyDisplayer() },
                { new HashSet<Type>(){
                    typeof(TechnologistDataModel)
                }, new EditableTechnologistDataModelDisplayer() },
                { new HashSet<Type>(){
                    typeof (DecimalMeasureWrapper)
                }, new DecimalMeasureWrapperEditablePropertyDisplayer() },
                { new HashSet<Type> ( ) {
                    typeof(OperatorFaceNumberModel)
                }, new EditableOperatorFaceNumberModelPropertyDisplayer() }
            };
            EnumPropertyDisplayer = new EditableComboBoxEnumDisplayer();
            ListPropertyDisplayer = new EditableMenuListDisplayer();
            CompositeTypeDisplayer = new EditableMenuCompositeTypesDisplayer();
        }

    }
}
