﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Terminal.Globals.Configuration;
using Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer;
using TerminalSharedDataModels.Attributes;

namespace Terminal.View.Service.MenuBuilder {
    public abstract class MenuBuilderBase : IMenuBuilder {

        public Action Redraw { get; set; }

        [Inject] public IConfigurationService _config { get; set; }

        protected Dictionary<HashSet<Type>, MenuBuilderPropertyDisplayerBase> PropertyTypeToDisplayerDict
            = new Dictionary<HashSet<Type>, MenuBuilderPropertyDisplayerBase>();
        protected MenuBuilderPropertyDisplayerBase EnumPropertyDisplayer;
        protected MenuBuilderPropertyDisplayerBase ListPropertyDisplayer;
        protected MenuBuilderPropertyDisplayerBase CompositeTypeDisplayer;


        public void CallRedraw() {
            Redraw?.Invoke();
        }

        private void InitPropertyDisplayerActions(MenuBuilderPropertyDisplayerBase target) {

            if (null == target) return;

            target.CallRedrawCallback = new Action( () => CallRedraw() );
            target.BuildMenuCallback = new Action<object[], IList<PropertyInfo>, IList<FieldInfo>, Panel, int, bool>(
                (o_target, o_pinfos, o_finfos, o_panel, o_left, o_displayHide) => {
                    BuildMenu( o_target, o_pinfos, o_finfos, o_panel, o_left, o_displayHide );
                }
            );
        }

        public virtual void BuildMenu(object[] target, IList<PropertyInfo> pinfos, IList<FieldInfo> finfos, Panel parent, int left = 0, bool IgnoreHide = false) {

            InitPropertyDisplayerActions( ListPropertyDisplayer );
            InitPropertyDisplayerActions( EnumPropertyDisplayer );
            InitPropertyDisplayerActions( CompositeTypeDisplayer );

            foreach (var pair in PropertyTypeToDisplayerDict)
                InitPropertyDisplayerActions( pair.Value );

            foreach (var pinfo in pinfos) {
                var hideAttr = Attribute.GetCustomAttribute( pinfo, typeof( HideInViewAttribute ) ) as HideInViewAttribute;
                if (null != hideAttr && !IgnoreHide) continue;

                var el = BuildMenuItem( target, pinfo, left, IgnoreHide );
                if (null != el) {
                    parent.Children.Add( el );
                }
            }
        }

        protected virtual UIElement BuildMenuItem(object[] target, PropertyInfo pinfo, int left = 0, bool IgnoreHide = false) {

            if (pinfo.GetType().IsEnum)
                return EnumPropertyDisplayer?.DisplayProperty( target, pinfo, left, IgnoreHide );

            if (pinfo.PropertyType.IsGenericType &&
                pinfo.PropertyType.GetGenericTypeDefinition() == typeof( List<> ))
                return ListPropertyDisplayer?.DisplayProperty( target, pinfo, left, IgnoreHide );

            foreach (var pair in PropertyTypeToDisplayerDict) {
                if (pair.Key.Contains( pinfo.PropertyType )) {
                    return pair.Value?.DisplayProperty( target, pinfo, left, IgnoreHide );
                }
            }

            return CompositeTypeDisplayer?.DisplayProperty(target,pinfo,left,IgnoreHide);

        }


        protected string GetPropertyDisplayName(PropertyInfo pinfo) {
            var propName = pinfo.Name;
            var displaynameAttr = Attribute.GetCustomAttribute( pinfo, typeof( DisplayNameAttribute ) )
                as DisplayNameAttribute;
            if (displaynameAttr != null)
                propName = displaynameAttr.DisplayName;
            return propName;
        }


    }
}
