﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Terminal.View.Service.MenuBuilder {
    /// <summary>
    /// Интерфейс сервиса построения UI меню
    /// </summary>
    public interface IMenuBuilder {

        Action Redraw { get; set; }

        void BuildMenu(
            object[] target,
            IList<PropertyInfo> pinfos,
            IList<FieldInfo> finfos,
            Panel parent,
            int left = 0,
            bool IgnoreHide = false
        );

    }
}
