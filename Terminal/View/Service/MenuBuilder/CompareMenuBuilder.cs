﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Terminal.Globals.Configuration;
using Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.CompareDisplayer;
using Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer;
using TerminalSharedDataModels.Attributes;
using TerminalSharedDataModels.DataModel.Wrapper;

namespace Terminal.View.Service.MenuBuilder {

    public class CompareMenuBuilder : MenuBuilderBase {

        public CompareMenuBuilder() {
            PropertyTypeToDisplayerDict = new Dictionary<HashSet<Type>, MenuBuilderPropertyDisplayer.MenuBuilderPropertyDisplayerBase>() {
                { new HashSet<Type>() {
                    typeof(int) ,
                    typeof(uint),
                    typeof(string),
                    typeof(float),
                    typeof(double),
                    typeof(byte),
                    typeof(decimal)
                } , new TextBoxCompareDisplayer()  },
                { new HashSet<Type>(){
                    typeof(bool)
                }, new ComboBoxCompareDisplayer() },
                { new HashSet<Type>(){
                    typeof (DecimalMeasureWrapper)
                }, new CompareDecimalWrapperPropertyDisplayer() }
            };
            EnumPropertyDisplayer = new EnumCompareDisplayer();
            ListPropertyDisplayer = new MenuListCompareDisplayer();
            CompositeTypeDisplayer = new MenuCompositeTypesCompareDisplayer();
        }


    }

}
