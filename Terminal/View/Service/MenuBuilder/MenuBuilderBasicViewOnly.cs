﻿using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Terminal.Globals.Configuration;
using Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.BasicViewDisplayer;
using Terminal.View.Service.MenuBuilder.MenuBuilderPropertyDisplayer.EditableDisplayer;
using TerminalSharedDataModels.Attributes;

namespace Terminal.View.Service.MenuBuilder {
    public class MenuBuilderBasicViewOnly : MenuBuilderBase {

        public MenuBuilderBasicViewOnly() {
            PropertyTypeToDisplayerDict = new Dictionary<HashSet<Type>, MenuBuilderPropertyDisplayer.MenuBuilderPropertyDisplayerBase>() {
                { new HashSet<Type>() {
                    typeof(int) ,
                    typeof(uint),
                    typeof(string),
                    typeof(float),
                    typeof(double),
                    typeof(byte),
                    typeof(decimal),
                    typeof(bool)
                } , new BasicTextDisplayer()  }
            };
            //EnumPropertyDisplayer = new EditableComboBoxEnumDisplayer();
            //ListPropertyDisplayer = new EditableMenuListDisplayer();
            //CompositeTypeDisplayer = new EditableMenuCompositeTypesDisplayer();
        }

    }
}
