﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Terminal.View.Service.WindowHeaverService {
    public interface IWindowHeaverService {

        IReadOnlyDictionary<string, Window> _registeredWindows {get;}
        void RegisterWindow ( string key, Window win );
        void UnregisterWindow ( string key );

    }
}
