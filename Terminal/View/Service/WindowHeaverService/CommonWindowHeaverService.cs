﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Terminal.View.Service.WindowHeaverService {
    public class CommonWindowHeaverService : IWindowHeaverService {

        public IReadOnlyDictionary<string,Window> _registeredWindows { get {
                return  _Windows ;
            } }
        private  Dictionary<string, Window> _Windows 
            = new Dictionary<string, Window> ( );


        public void RegisterWindow ( string key, Window win ) {
            if(!_Windows.ContainsKey(key))
                _Windows.Add ( key, win );
            else {
                _Windows[key].Close ( );
                _Windows[key] = win;
            }
        }

        public void UnregisterWindow(string key ) {
            if ( _Windows.ContainsKey ( key ) ) {
                _Windows[key].Close ( );
                _Windows.Remove ( key );
            }
        }

    }
}
