﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Terminal.View.Service.VirtualKeyboardInputService {

    public interface IVirtualKeyboardInputService {

        void ShowAt(TextBox tb);

        void _Hide();

    }

}
