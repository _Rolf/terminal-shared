﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Terminal.View.Service.WindowHeaverService;

namespace Terminal.View.Service.VirtualKeyboardInputService {
    /// <summary>
    /// Interaction logic for VirtualAlphathetKeyboardInputService.xaml
    /// </summary>
    public partial class VirtualAlphathetKeyboardInputService : Window, IVirtualKeyboardInputService {

        #region Блокировка закрытия и движения окна (свистопляска с вызовами windows)

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [System.Runtime.InteropServices.DllImport ( "user32.dll", SetLastError = true )]
        private static extern int GetWindowLong ( IntPtr hWnd, int nIndex );
        [System.Runtime.InteropServices.DllImport ( "user32.dll" )]
        private static extern int SetWindowLong ( IntPtr hWnd, int nIndex, int dwNewLong );

        private void MainView_Loaded ( object sender, RoutedEventArgs e ) {
            var hwnd = new System.Windows.Interop.WindowInteropHelper ( this ).Handle;
            SetWindowLong ( hwnd, GWL_STYLE, GetWindowLong ( hwnd, GWL_STYLE ) & ~WS_SYSMENU );
        }

        public VirtualAlphathetKeyboardInputService() {
            InitializeComponent();
            Loaded += MainView_Loaded;
        }

        #endregion

        protected TextBox tb_target;

        public void ShowAt(TextBox tb) {
            tb_target = tb;
            this.Topmost = true;

            var locFromScreen = tb.PointToScreen(new Point(0, 0));
            this.Left = locFromScreen.X - 630;
            this.Top = locFromScreen.Y + 30;

            var winHeaver = Globals.Inject.InjectService.InjectService.instance.Get<IWindowHeaverService> ( );
            winHeaver.RegisterWindow ( "Virtual Keyboard", this );

            try {
                Show();
            } catch (Exception) {
                new VirtualAlphathetKeyboardInputService().ShowAt(tb);
            }
        }

        public void _Hide() {
            var winHeaver = Globals.Inject.InjectService.InjectService.instance.Get<IWindowHeaverService> ( );
            winHeaver.UnregisterWindow ( "Virtual Keyboard" );
        }

        private void bt_Close_Click(object sender, RoutedEventArgs e) {
            _Hide();
        }

        private void bt_1_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("1");
        }

        private void bt_2_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("2");
        }

        private void bt_3_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("3");
        }

        private void bt_4_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("4");
        }

        private void bt_5_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("5");
        }

        private void bt_6_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("6");
        }

        private void bt_7_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("7");
        }

        private void bt_8_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("8");
        }

        private void bt_9_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("9");
        }

        private void bt_0_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("0");
        }

        private void bt_q_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("й");
        }

        private void bt_w_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("ц");
        }

        private void bt_e_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("у");
        }

        private void bt_r_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("к");
        }

        private void bt_t_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("е");
        }

        private void bt_y_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("н");
        }

        private void bt_u_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("г");
        }

        private void bt_i_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("ш");
        }

        private void bt_o_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("щ");
        }

        private void bt_p_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("з");
        }

        private void bt_ha_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("х");
        }

        private void bt_Y_Click_1(object sender, RoutedEventArgs e) {
            tb_target.AppendText("ъ");
        }

        private void bt_a_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("ф");
        }

        private void bt_s_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("ы");
        }

        private void bt_d_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("в");
        }

        private void bt_f_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("а");
        }

        private void bt_g_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("п");
        }

        private void bt_h_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("р");
        }

        private void bt_j_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("о");
        }

        private void bt_k_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("л");
        }

        private void bt_l_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("д");
        }

        private void bt_G_Click_1(object sender, RoutedEventArgs e) {
            tb_target.AppendText("ж");
        }

        private void bt_A_Click_1(object sender, RoutedEventArgs e) {
            tb_target.AppendText("э");
        }

        private void bt_z_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("я");
        }

        private void bt_x_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("ч");
        }

        private void bt_c_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("с");
        }

        private void bt_v_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("м");
        }

        private void bt_b_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("и");
        }

        private void bt_n_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("т");
        }

        private void bt_m_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText("ь");
        }

        private void bt_B_Click_1(object sender, RoutedEventArgs e) {
            tb_target.AppendText("б");
        }

        private void bt_U_Click_1(object sender, RoutedEventArgs e) {
            tb_target.AppendText("ю");
        }

        private void bt_comma_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText(",");
        }

        private void bt_space_Click(object sender, RoutedEventArgs e) {
            tb_target.AppendText(" ");
        }

        private void bt_backspace_Click(object sender, RoutedEventArgs e) {
            tb_target.Text = new string(tb_target.Text.Take(tb_target.Text.Length - 1).ToArray());
        }
    }
}
