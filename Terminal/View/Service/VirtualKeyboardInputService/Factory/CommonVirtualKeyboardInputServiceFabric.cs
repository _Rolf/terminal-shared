﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.View.Service.VirtualKeyboardInputService.Factory {
    public class CommonVirtualKeyboardInputServiceFabric : IVirtualKeyboardInputServiceFabric {

        public IVirtualKeyboardInputService GenerateService ( Type propertyType ) {
            if ( propertyType == typeof ( string ) )
                return new VirtualAlphathetKeyboardInputService ( );
            else return new VirtualNumericKeyboardInputService ( );
        }

    }
}
