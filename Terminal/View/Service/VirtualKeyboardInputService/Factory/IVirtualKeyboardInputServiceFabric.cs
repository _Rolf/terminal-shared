﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.View.Service.VirtualKeyboardInputService.Factory {
    public interface IVirtualKeyboardInputServiceFabric {

        IVirtualKeyboardInputService GenerateService ( Type propertyType );

    }
}
