﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Terminal.View.Service.WindowHeaverService;

namespace Terminal.View.Service.VirtualKeyboardInputService
{
    /// <summary>
    /// Логика взаимодействия для VirtualNumericKeyboardInputService.xaml
    /// </summary>
    public partial class VirtualNumericKeyboardInputService : Window , IVirtualKeyboardInputService{

        #region Блокировка закрытия и движения окна (свистопляска с вызовами windows)

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [System.Runtime.InteropServices.DllImport ( "user32.dll", SetLastError = true )]
        private static extern int GetWindowLong ( IntPtr hWnd, int nIndex );
        [System.Runtime.InteropServices.DllImport ( "user32.dll" )]
        private static extern int SetWindowLong ( IntPtr hWnd, int nIndex, int dwNewLong );


        public VirtualNumericKeyboardInputService (){
            InitializeComponent();
            Loaded += MainView_Loaded;
        }


        private void MainView_Loaded ( object sender, RoutedEventArgs e ) {
            var hwnd = new System.Windows.Interop.WindowInteropHelper ( this ).Handle;
            SetWindowLong ( hwnd, GWL_STYLE, GetWindowLong ( hwnd, GWL_STYLE ) & ~WS_SYSMENU );
        }

        #endregion


        protected TextBox tb_target;

        public void ShowAt ( TextBox tb ) {
            tb_target = tb;
            this.Topmost = true;

            var locFromScreen = tb.PointToScreen ( new Point ( 0, 0 ) );
            this.Left = locFromScreen.X - 300;
            this.Top = locFromScreen.Y + 30;

            var winHeaver = Globals.Inject.InjectService.InjectService.instance.Get<IWindowHeaverService> ( );
            winHeaver.RegisterWindow ( "Virtual Keyboard", this );

            try {
                Show ( );
            } catch ( Exception ) {
                new VirtualNumericKeyboardInputService ( ).ShowAt(tb);
            }
        }

        public void _Hide ( ) {
            var winHeaver = Globals.Inject.InjectService.InjectService.instance.Get<IWindowHeaverService> ( );
            winHeaver.UnregisterWindow ( "Virtual Keyboard" );

        }

        private void bt_Close_Click ( object sender, RoutedEventArgs e ) {
            _Hide ( );
        }

        private void bt_1_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "1" );
        }

        private void bt_2_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "2" );
        }

        private void bt_3_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "3" );
        }

        private void bt_4_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "4" );
        }

        private void bt_5_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "5" );
        }

        private void bt_6_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "6" );
        }

        private void bt_7_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "7" );
        }

        private void bt_8_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "8" );
        }

        private void bt_9_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "9" );
        }

        private void bt_0_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "0" );
        }

        private void bt_comma_Click ( object sender, RoutedEventArgs e ) {
            tb_target.AppendText ( "," );
        }

        private void bt_backspace_Click ( object sender, RoutedEventArgs e ) {
            tb_target.Text = new string ( tb_target.Text.Take ( tb_target.Text.Length - 1 ).ToArray ( ) );
        }

    }
}
