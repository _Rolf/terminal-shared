﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.View {
    public interface IView {

        void _Display();

        void _Hide();

        Action Update { get; set; }

    }
}
